<?php

namespace Drupal\bazaarvoice\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class BazaarvoiceAdminSettingsForm.
 *
 * @package Drupal\bazaarvoice\Form
 */
class AdminSettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'bazaarvoice_admin_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return ['bazaarvoice.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $config = $this->config('bazaarvoice.settings');


        $form['mode'] = [
          '#type' => 'select',
          '#title' => $this->t('Account Mode'),
          '#description' => $this->t('Mode to use for connecting to Bazaarvoice. Use staging for pre-production development and testing.'),
          '#options' => [
            'stg' => $this->t('Staging'),
            'prod' => $this->t('Production'),
          ],
          '#default_value' => $config->get('mode'),
        ];

        $form['method'] = [
          '#type' => 'select',
          '#title' => $this->t('Method'),
          '#description' => $this->t('Method to use to connect to Bazaarvoice.'),
          '#options' => [
            'conversations' => $this->t('Conversations API'),
            'hosted' => $this->t('Hosted UI (javascript)'),
          ],
          '#default_value' => $config->get('method'),
        ];

        $form['conversations'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Conversations API settings'),
          '#description' => $this->t('Settings needed for using the Bazaarvoice conversations API'),
          '#states' => [
            'visible' => [
              ':input[name="method"]' => ['value' => 'conversations'],
            ],
          ],
        ];

        $form['conversations']['apiKey'] = [
          '#type' => 'textfield',
          '#title' => $this->t('API Key'),
          '#description' => $this->t('API key is required to authenticate API user and check permission to access particular client data.  If you do not have an API key you can get one @url', [
            '@url' => Link::fromTextAndUrl(t('here'), Url::fromUri('https://developer.bazaarvoice.com/apps/register'))
              ->toString(),
          ]),
          '#size' => 40,
          '#maxlength' => 255,
          '#default_value' => $config->get('conversations.apiKey'),
          '#states' => [
            'required' => [
              ':input[name="method"]' => ['value' => 'conversations'],
            ],
          ],
        ];

        $form['conversations']['cache_limit'] = [
          '#type' => 'select',
          '#title' => $this->t('API Request Cache'),
          '#description' => $this->t('Select how long to cache results of API queries.'),
          '#options' => [
            0 => $this->t('Never'),
            3600 => $this->t('One Hour'),
            86400 => $this->t('One Day'),
          ],
          '#default_value' => $config->get('conversations.cache_limit'),
        ];


        $form['hosted'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Hosted UI settings'),
          '#description' => $this->t('Settings needed for Bazaarvoice hosted reviews'),
          '#states' => [
            'visible' => [
              ':input[name="method"]' => [
                'value' => 'hosted',
              ],
            ],
          ],
        ];

        $form['hosted']['client_name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Name'),
          '#required' => TRUE,
          '#description' => $this->t('The client name provided by Bazaarvoice. Remember that this value is case sensitive.'),
          '#default_value' => $config->get('hosted.client_name'),
          '#states' => [
            'required' => [
              ':input[name="method"]' => ['value' => 'hosted'],
            ],
          ],
        ];

        $form['hosted']['site_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Site Id'),
          '#require' => TRUE,
          '#description' => $this->t('The ID of the deployment zone you want to use. This is set in the Bazaarvoice configuration hub within the Bazaarvoice workbench.'),
          '#default_value' => $config->get('hosted.site_id'),
          '#states' => [
            'required' => [
              ':input[name="method"]' => ['value' => 'hosted'],
            ],
          ],
        ];

        $form['hosted']['shared_key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Shared Key'),
          '#description' => $this->t('Bazaarvoice shared encoding key to create a UAS token'),
          '#default_value' => $config->get('hosted.shared_key'),
        ];

        $form['hosted']['api_version'] = [
          '#type' => 'select',
          '#title' => $this->t('API Version'),
          '#description' => $this->t('Which API to use?'),
          '#options' => [
            'bv_api' => $this->t('bvapi.js (older)'),
            'bv' => $this->t('bv.js (newer)'),
          ],
          '#default_value' => $config->get('hosted.api_version'),
        ];

        $languages = \Drupal::languageManager()->getLanguages();

        $form['locale'] = [
          '#type' => 'details',
          '#title' => $this->t('Bazaarvoice Locale Codes'),
          '#open' => TRUE,
        ];

        $caption = $this->t('Map Languages to Bazaarvoice Locale Code (format of : xx_YY). Please refer to the Bazaarvoice <a href="@bv_link">supported locales</a>.', ['@bv_link' => 'https://developer.bazaarvoice.com/conversations-api/tutorials/internationalization#supported-locales']);
        $form['locale']['locale_codes'] = [
          '#type' => 'table',
          '#caption' => $caption,
          '#header' => [
            $this->t('Language'),
            $this->t('Locale Code'),
          ],
          '#tree' => TRUE,
        ];

        foreach ($languages as $language) {
            $form['locale']['locale_codes'][$language->getId()]['language'] = [
              '#markup' => $language->getName(),
            ];
            $form['locale']['locale_codes'][$language->getId()]['locale_code'] = [
              '#type' => 'textfield',
              '#maxlength' => 6,
              '#size' => 6,
              '#required' => TRUE,
              '#default_value' => \Drupal::service('bazaarvoice')
                ->getBazaarvoiceLocaleCode($language->getId()),
            ];
        }

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        // Test submitted locale codes are valid.
        if (isset($values['locale_codes'])) {
            $bazaarvoice = \Drupal::service('bazaarvoice');
            foreach ($values['locale_codes'] as $language => $settings) {
                $locale_code = $settings['locale_code'];

                if (!$bazaarvoice->isValidLocaleCode($locale_code)) {
                    $form_state->setErrorByName('locale_codes][' . $language . '][locale_code', $this->t('@language Locale code is invalid must match the format of xx_YY.', [
                      '@language' => \Drupal::languageManager()
                        ->getLanguage($language)
                        ->getName(),
                    ]));
                }
            }
        }
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();

        $this->configFactory()->getEditable('bazaarvoice.settings')
          ->set('mode', $values['mode'] == 'prod' ? 'prod' : 'stg')
          ->set('method', $values['method'] == 'conversations' ? 'conversations' : 'hosted')
          ->set('conversations.apiKey', $values['apiKey'])
          ->set('conversations.cache_limit', $values['cache_limit'])
          ->set('hosted.client_name', $values['client_name'])
          ->set('hosted.site_id', $values['site_id'])
          ->set('hosted.shared_key', $values['shared_key'])
          ->set('hosted.api_version', $values['api_version'])
          ->save();

        $bazaarvoice = \Drupal::service('bazaarvoice');

        // Get old locale codes.
        $locale_codes = $bazaarvoice->getLocaleCodes();
        $updated_codes = [];

        // Get new locale codes.
        foreach ($values['locale_codes'] as $language => $language_settings) {
            $locale_code = $language_settings['locale_code'];

            if (!isset($locale_codes[$language]) || ($locale_codes[$language] != $locale_code)) {
                $updated_codes[$language] = $locale_code;
            }
        }

        // Have locale codes to update?
        if (!empty($updated_codes)) {
            $bazaarvoice->setLocaleCodes($updated_codes);
        }

        parent::submitForm($form, $form_state);
    }

}
