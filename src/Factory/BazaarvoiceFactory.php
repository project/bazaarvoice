<?php

namespace Drupal\bazaarvoice\Factory;

use GuzzleHttp\ClientInterface;
use BazaarvoiceRequest\Request\BazaarvoiceRequestInterface;
use BazaarvoiceRequest\Request\BazaarvoiceRequest;
use BazaarvoiceConversations\BazaarvoiceConversations;

class BazaarvoiceFactory {

  public static function createRequest($config, ClientInterface $client) {

    $api_key = $config->get('bazaarvoice.settings')->get('conversations.apiKey');
    $bazaarvoiceRequest = new BazaarvoiceRequest($client, $api_key);

    if ($config->get('bazaarvoice.settings')->get('mode') == 'stg') {
      $bazaarvoiceRequest->useStage();
    }
    return $bazaarvoiceRequest;
  }

  public static function createConversations(BazaarvoiceRequestInterface $request) {
    return new BazaarvoiceConversations($request);
  }

}