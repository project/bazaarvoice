<?php

namespace Drupal\bazaarvoice\Service;

use Drupal\Core\Entity\EntityInterface;

interface BazaarvoiceServiceInterface {

  public function getEntityLocale(EntityInterface $entity);

  public function getBazaarvoiceLocaleCode($locale);
}