<?php

namespace Drupal\bazaarvoice\Service;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class BazaarvoiceService implements BazaarvoiceServiceInterface {

    protected $config;

    protected $database;

    protected $apiVersion;

    protected $langaugeManager;

    public function __construct($config, Connection $database, LanguageManagerInterface $languageManager) {
        $this->config = $config->get('bazaarvoice.settings');
        $this->languageConfig = $config->get('bazaarvoice.locales');
        $this->apiVersion = $config->get('bazaarvoice.settings')
          ->get('hosted.api_version');
        $this->database = $database;
        $this->languageManager = $languageManager;
    }

    public function getCacheLimit() {
        return $this->config->get('conversations.cache_limit');
    }

    public function getEntityLocale(EntityInterface $entity) {
        $langcode = method_exists($entity, 'language') ? $entity->language()
          ->getId() : $this->langaugeManager->getCurrentLanguage()->getId();
        return $this->getBazaarvoiceLocaleCode($langcode);
    }

    /**
     * {@inheritdoc}
     */
    public function getBazaarvoiceLocaleCode($langcode) {
        $locale_code = FALSE;
        if ($locale_codes = $this->getLocaleCodes()) {
            if (isset($locale_codes[$langcode])) {
                $locale_code = $locale_codes[$langcode];
            }
        }
        return $locale_code;
    }

    public function setLocaleCodes($locales) {
        //@todo: dependency injection.
        \Drupal::configFactory()->getEditable('bazaarvoice.locales')
          ->set('map', $locales)
          ->save();
    }

    public function getLocaleCodes() {
        $result = $this->languageConfig->get('map');
        return $result;
    }

    public function isValidLocaleCode($locale_code) {
        return preg_match('/[a-z]{2}_[A-Z]{2}/', $locale_code);
    }

    public function buildHostedJsPath($locale_code = NULL) {

        $mode = '';
        if ($this->apiVersion == 'bv') {
            $mode = 'production';
            if ($this->config->get('mode') == 'stg') {
                $mode = 'staging';
            }
        }
        elseif ($this->config->get('mode') == 'stg') {
            $mode = 'bvstaging/';
        }

        $client_name = $this->config->get('hosted.client_name');
        $site_id = $this->config->get('hosted.site_id');

        if (!$locale_code || !$this->isValidLocaleCode($locale_code)) {
            $language = \Drupal::languageManager()
              ->getCurrentLanguage()
              ->getId();
            $locale_code = \Drupal::service('bazaarvoice')
              ->getBazaarvoiceLocaleCode($language);
        }

        $js_path = '//display.ugc.bazaarvoice.com/' . $mode . 'static/' . $client_name . '/' . $locale_code . '/bvapi.js';

        if ($this->apiVersion == 'bv') {
            $js_path = '//apps.bazaarvoice.com/deployments/' . $client_name . '/' . $site_id . '/' . $mode . '/' . $locale_code . '/bv.js';
        }
        return $js_path;
    }
}
