<?php

namespace Drupal\bazaarvoice_products\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Product Statistics' Block.
 *
 * @Block(
 *   id = "bazaarvoice_product_statistics",
 *   admin_label = @Translation("Product Statistics"),
 * )
 */
class BazaarvoiceProductsBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $block = [];

        // has permission?
        $has_permission = \Drupal::currentUser()
          ->hasPermission('view bazaarvoice product statistics');

        if ($has_permission && ($entity = $this->getCurrentRouteProductEntity())) {
            if ($statistics = \Drupal::service('bazaarvoice.products')
              ->renderEntityProductStatistics($entity)) {
                $block['subject'] = t('Product Statistics');
                $block['content'] = $statistics;
            }
        }

        return $block;
    }

    public function getCurrentRouteProductEntity() {
        $entity = FALSE;

        $parameters = \Drupal::routeMatch()->getParameters();
        if ($parameters->count() > 0) {
            // Assume first parameter is the entity type.
            $keys = $parameters->keys();
            $entity_type = array_pop($keys);
            // Get Entity.
            $entity = $parameters->get($entity_type);
            // Try to load the entity.
            $bundle = $entity->bundle();
            if (!\Drupal::service('bazaarvoice.products')
              ->isProductType($entity_type, $bundle)) {
                $entity = FALSE;
            }
        }
        return $entity;
    }

}
