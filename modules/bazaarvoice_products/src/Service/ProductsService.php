<?php

namespace Drupal\bazaarvoice_products\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Database\Connection;
use BazaarvoiceConversations\BazaarvoiceConversationsInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\bazaarvoice\Service\BazaarvoiceServiceInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ProductsService implements ProductsServiceInterface {

    use UseCacheBackendTrait;
    use StringTranslationTrait;

    protected $config;

    protected $database;

    protected $moduleHandler;

    protected $bundleInfo;

    protected $entityTypeManager;

    protected $loggerFactory;

    protected $bazaarvoice;

    protected $method;

    protected $apiVersion;

    protected $products;

    protected $product_types;

    public function __construct($config, Connection $database, ModuleHandlerInterface $module_handler, EntityTypeBundleInfoInterface $bundle_info, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, CacheBackendInterface $cache_backend, BazaarvoiceServiceInterface $bazaarvoice, BazaarvoiceConversationsInterface $conversations) {

        $this->config = $config;
        $this->database = $database;
        $this->method = $config->get('bazaarvoice.settings')->get('method');
        $this->apiVersion = $config->get('bazaarvoice.settings')
          ->get('hosted.api_version');
        $this->moduleHandler = $module_handler;
        $this->bundleInfo = $bundle_info;
        $this->entityTypeManager = $entity_type_manager;
        $this->loggerFactory = $logger_factory->get('bazaarvoice_products');
        $this->bazaarvoice = $bazaarvoice;
        if ($this->method == 'conversations') {
            $this->products = $conversations->getContentType('Products');
        }
        else {
            $this->products = FALSE;
        }
    }

    public function isProductType($entity_type, $bundle = FALSE) {
        $is_type = FALSE;
        if ($bundle) {
            if ($this->getProductType($entity_type, $bundle)) {
                $is_type = TRUE;
            }
        }
        elseif ($this->getProductTypes($entity_type)) {
            $is_type = TRUE;
        }

        return $is_type;

    }

    public function getProductType($entity_type, $bundle_name) {
        $product_type = FALSE;

        if ($product_types = $this->getProductTypes($entity_type)) {
            if (isset($product_types[$entity_type][$bundle_name])) {
                $product_type = $product_types[$entity_type][$bundle_name];
            }
        }

        return $product_type;
    }

    public function getProductTypes($entity_type = NULL) {

        if (!$this->product_types) {
            $product_types = [];
            $entity_types = $this->bundleInfo->getAllBundleInfo();
            foreach ($entity_types as $type => $bundles) {
                foreach ($bundles as $bundle => $info) {
                    if ($entity = $this->getEntityTypeObject($type, $bundle)) {
                        // Get 3rd party settings.
                        if ($product_id_field = $entity->getThirdPartySetting('bazaarvoice_products', 'product_id_field', FALSE)) {
                            $product_types[$type][$bundle] = [
                              'entity_type' => $type,
                              'entity_bundle' => $bundle,
                              'product_id_field' => $product_id_field,
                            ];
                        }
                    }
                }
            }
            if (!empty($product_types)) {
                $this->product_types = $product_types;
            }
        }
        else {
            $product_types = $this->product_types;
        }

        if ($entity_type) {
            if (isset($product_types[$entity_type])) {
                $product_types = [
                  $entity_type => $product_types[$entity_type],
                ];
            }
            else {
                $product_types = [];
            }
        }

        return $product_types;
    }

    public function setProductType($entity_type, $bundle_name, $id_field) {
        if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
            $entity->setThirdPartySetting('bazaarvoice_products', 'product_id_field', $id_field);
            return $entity->save();
        }

        return FALSE;
    }

    public function removeProductType($entity_type, $bundle_name = FALSE) {
        if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
            $entity->unsetThirdPartySetting('bazaarvoice_products', 'product_id_field');
            $entity->save();
        }
    }

    public function getProductIdFields($entity_type, $bundle_name) {
        // Retrieve field options from modules.
        $product_id_fields = $this->moduleHandler->invokeAll('bazaarvoice_products_id_fields', [
          $entity_type,
          $bundle_name,
        ]);

        // Allow modules to alter.
        $this->moduleHandler->alter('bazaarvoice_products_id_fields', $product_id_fields, $type, $bundle);

        return $product_id_fields;
    }

    public function getEntityProductStatistics(EntityInterface $entity) {

        $statistics = FALSE;
        // Need to get the product_id value.
        if ($this->products && ($product_id = $this->getEntityProductId($entity))) {
            $locale = $this->bazaarvoice->getEntityLocale($entity);

            $cid = 'bazaarvoice_products_statistics:' . $product_id . ':' . $locale;

            // Able to get from cache?
            if ($cache = $this->cacheGet($cid)) {
                $statistics = $cache->data;
            }
            else {
                $configuration = [
                  'arguments' => [
                    'stats' => 'reviews',
                    'Locale' => $locale,
                  ],
                ];

                if ($raw_statistics = $this->products->getResultById($product_id, $configuration)) {
                    $statistics = [
                      'review_count' => $raw_statistics['ReviewStatistics']['TotalReviewCount'],
                      'average_rating' => $raw_statistics['ReviewStatistics']['AverageOverallRating'],
                      'rating_range' => $raw_statistics['ReviewStatistics']['OverallRatingRange'],
                      'raw_statistics' => $raw_statistics,
                    ];
                    $this->cacheSet($cid, $statistics, $this->bazaarvoice->getCacheLimit());
                }
            }
        }
        return $statistics;
    }

    public function renderEntityProductStatistics(EntityInterface $entity) {
        $content = '';
        // Get statistics.
        if ($this->method == 'conversations') {

            if ($statistics = $this->getEntityProductStatistics($entity)) {
                $content = [
                  '#theme' => 'bazaarvoice_product_statistics',
                  '#review_count' => $statistics['review_count'],
                  '#rating' => $this->t('@rating out of @range', [
                    '@rating' => round($statistics['average_rating'], 2),
                    '@range' => $statistics['rating_range'],
                  ]),
                  '#average_rating' => $statistics['average_rating'],
                  '#rating_range' => $statistics['rating_range'],
                  '#statistics' => $statistics,
                  '#product_info' => $this->getProductType($entity->getEntityTypeId(), $entity->bundle()),
                ];
            }
        }
        else {
            $productId = $this->getEntityProductId($entity);
            $content = [
              '#theme' => 'bazaarvoice_product_hosted_statistics',
              '#product_info' => $this->getProductType($entity->getEntityTypeId(), $entity->bundle()),
              '#attached' => [
                'library' => [
                  'bazaarvoice/hosted.external',
                  'bazaarvoice_products/hosted.inline_ratings',
                ],
                'drupalSettings' => [
                  'bazaarvoiceProducts' => [
                    'productIds' => [
                      $productId => [
                        'url' => $entity->toUrl()
                          ->setAbsolute(TRUE)
                          ->toString(),
                      ],
                    ],
                    'apiVersion' => $this->apiVersion,
                  ],
                ],
              ],
              '#productId' => $productId,
              '#apiVersion' => $this->apiVersion,
            ];
        }

        return $content;
    }

    public function getEntityProductId(EntityInterface $entity) {
        $product_id = FALSE;

        // Have product type definition?
        if ($product_type = $this->getProductType($entity->getEntityTypeId(), $entity->bundle())) {
            // Able to get a value?
            if ($entity->hasField($product_type['product_id_field']) && ($value = $entity->get($product_type['product_id_field'])->value)) {
                // format the value.
                $product_id = $this->formatProductId($value);
            }
        }

        return $product_id;
    }

    public function getEntityTypeFormFields($entity_type, $bundle_name) {
        $form_fields = FALSE;
        // Have fields that can be used as a product id?
        if ($type_fields = $this->getProductIdFields($entity_type, $bundle_name)) {
            $form_fields = [];
            // Get current definition.
            $product_type = $this->getProductType($entity_type, $bundle_name);

            // Build the product checkbox.
            $form_fields['bazaarvoice_product_type'] = [
              '#type' => 'checkbox',
              '#title' => t('Use as a bazaarvoice product.'),
              '#description' => t('Enable content of this type to be used as Bazaarvoice products.'),
              '#weight' => 0,
              '#default_value' => $product_type ? TRUE : FALSE,
            ];

            // Build selector for product id field.
            $form_fields['bazaarvoice_product_id'] = [
              '#type' => 'select',
              '#title' => t('Product ID Field'),
              '#attributes' => ['class' => ['form-required']],
              '#description' => t('Select the field that holds the Bazaarvoice Product ID value.'),
              '#options' => $this->getProductIdFields($entity_type, $bundle_name),
              '#states' => [
                'visible' => [
                  ':input[name=bazaarvoice_product_type]' => [
                    'checked' => TRUE,
                  ],
                ],
                'required' => [
                  ':input[name=bazaarvoice_product_type]' => [
                    'checked' => TRUE,
                  ],
                ],
              ],
              '#default_value' => isset($product_type['product_id_field']) ? $product_type['product_id_field'] : '',
            ];
        }

        return $form_fields;
    }

    public function submitEntityTypeFormFields($entity_type, $bundle_name, array $values) {

        if (isset($values['bazaarvoice_product_type']) && $values['bazaarvoice_product_type']) {
            // Get the old product type data.
            $product_type = $this->getProductType($entity_type, $bundle_name);

            // Save the new product type data.
            $this->setProductType($entity_type, $bundle_name, $values['bazaarvoice_product_id']);

            // If old product_id field and it has changed.
            if (isset($product_type['product_id_field']) && ($product_type['product_id_field'] != $values['bazaarvoice_product_id'])) {
                // Log the event.
                $this->loggerFactory->notice('Product ID changed from @old_id to @new_id', [
                  '@old_id' => $product_type['product_id_field'],
                  '@new_id' => $values['bazaarvoice_product_id'],
                ]);
            }
        }
        // No product type, so delete any if it exists.
        elseif ($this->isProductType($entity_type, $bundle_name)) {
            $this->removeProductType($entity_type, $bundle_name);
            // Log the event.
            $this->loggerFactory->notice('Product Configuration removed for @type @bundle', [
              '@type' => $entity_type,
              '@bundle' => $bundle_name,
            ]);
        }
    }

    public function formatProductId($raw_value) {
        // @TODO: improve this code.
        $product_id = trim($raw_value);
        $product_id = strtolower($product_id);
        $product_id = preg_replace('/[^a-z0-9\s\-\_\*]+/', '_', $product_id);
        $product_id = mb_convert_encoding($product_id, "HTML-ENTITIES", "UTF-8");
        $product_id = preg_replace('/\s+/', '_', $product_id);
        $product_id = trim($product_id, '_');
        $product_id = str_replace([
          '&#x2122;',
          '&trade;',
          '&#174;',
          '&#xae;',
          '&reg;',
          '&#x2120;',
          '&#169;',
          '&copy;',
          '&#8482;',
          '&#39;',
          '&apos;',
          '&#146;',
          '&rsquo;',
          '&acute;',
          '&#41;',
          '&#39;',
          '&apos;',
          '&#146;',
          '&rsquo;',
          '&#x2019;',
          '&#8217;',
        ], '', $product_id);
        $product_id = str_replace([
          ' ',
          '<',
          '>',
          '/',
          '\'',
          ',',
          '.',
          ';',
          ':',
          '&#10;',
          '#',
          '&#35;',
          '(',
          '&#40;',
          ')',
          '\'',
        ], '_', $product_id);
        $product_id = str_replace(['&', '&#38;', '&amp;'], 'and', $product_id);
        $product_id = str_replace([
          '__',
          '&mdash;',
          '&#151;',
        ], '-', $product_id);
        $product_id = trim($product_id, '_');
        return $product_id;
    }

    private function getEntityTypeObject($entity_type, $bundle_name) {
        $entity = FALSE;
        if ($type_storage = $this->entityTypeManager->getStorage($entity_type)
          ->getEntityType()
          ->getBundleEntityType()) {
            $entity = $this->entityTypeManager->getStorage($type_storage)
              ->load($bundle_name);
        }
        return $entity;
    }

}
