<?php

namespace Drupal\bazaarvoice_products\Service;

use Drupal\Core\Entity\EntityInterface;

interface ProductsServiceInterface {

  /**
   * Returns a boolean check if an entity type and/or bundle is a Product.
   *
   * @param string $entity_type
   *   The name of the entity type
   *
   * @param bool|string $bundle_name
   *   (Optional) name of bundle.
   *
   * @return bool
   *  Boolean TRUE or FALSE.
   */
  public function isProductType($entity_type, $bundle_name = FALSE);

  /**
   * Returns Bazaarvoice product mapping for an entity type bundle.
   *
   * @param string $entity_type
   *   The name of the entity type.
   *
   * @param string $bundle_name
   *   The name of the bundle.
   *
   * @return array|bool
   *   Array of product type data or boolean FALSE.
   */
  public function getProductType($entity_type, $bundle_name);

  /**
   * Returns an array of Entity types and Entity bundles that are marked as
   * Product types.
   *
   * @return array
   *  Array of product types. Keyed by Entity Type => Bundle Name => ID field.
   */
  public function getProductTypes();

  /**
   * Saves configuration for an entity bundle to be a product type.
   *
   * @param string $entity_type
   *   The name of the entity type.
   *
   * @param string $bundle_name
   *   The name of the bundle.
   *
   * @param string $id_field
   *   The entity field to be used as the product ID.
   *
   * @return bool
   *   Return boolean TRUE|FALSE on success.
   */
  public function setProductType($entity_type, $bundle_name, $id_field);

  /**
   * Removes configuration for an entity (bundle) product type.
   *
   * @param string $entity_type
   *   The name of the entity type
   *
   * @param bool|string $bundle_name
   *   (Optional) name of bundle.
   *
   * @return bool
   *   Return boolean TRUE|FALSE on success.
   */
  public function removeProductType($entity_type, $bundle_name = FALSE);

  /**
   * Returns array of entity fields that can be used for a product id.
   *
   * @param string $entity_type
   *   The name of the entity type
   *
   * @param bool|string $bundle_name
   *   (Optional) name of bundle.
   *
   * @return array
   *   Key/Value array of fields. Key is field id, value is field name.
   */
  public function getProductIdFields($entity_type, $bundle_name);

  /**
   * Return array of Bazaarvoice product statistics for given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity to fetch statistics for.
   *
   * @return array|bool
   *   Array of staistical values or boolean false.
   */
  public function getEntityProductStatistics(EntityInterface $entity);

  /**
   * Return the Product Id value for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity to fetch productId for.
   *
   * @return mixed
   *   ProductID string or boolean FALSE.
   */
  public function getEntityProductId(EntityInterface $entity);

}