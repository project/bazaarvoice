<?php

namespace Drupal\bazaarvoice_productfeed;


use BazaarvoiceProductFeed\Elements\FeedElementInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

class ProductFeed extends \BazaarvoiceProductFeed\ProductFeed {

  /*
   * Override parent method for saving a file.
   */
  public function saveFeed(FeedElementInterface $feed, $directory, $filename) {
    $saved = FALSE;
    // Get XML string of feed.
    if ($feed_xml = $this->printFeed($feed)) {
      // gzip file.
      if ($xml_file = gzencode($feed_xml)) {
        // Build Drupal URI path.
        $file_uri = \Drupal::config('system.file')->get('default_scheme') . '://' . $directory . '/' . $filename . '.xml.gz';
        $file_directory = StreamWrapperManagerInterface::normalizeUri(\Drupal::config('system.file')->get('default_scheme'). '://' . $directory);

        // Prepare file directory.
        if (FileSystemInterface::prepareDirectory($file_directory, FileSystemInterface::CREATE_DIRECTORY)) {

          $file_path = StreamWrapperManagerInterface::normalizeUri($file_uri);
          // Save the file as unmangaged.
          if ($unmanaged_file = FileSystemInterface::saveData($xml_file, $file_path, FileSystemInterface::EXISTS_REPLACE)) {
            // Return full file location?
            $saved = file_create_url($unmanaged_file);
          }
        }
        else {
          // @TODO: return error, unable to create directory.
        }
      }
    }
    return $saved;
  }
}
