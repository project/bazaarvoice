<?php

namespace Drupal\bazaarvoice_productfeed\Service;


use BazaarvoiceProductFeed\Elements\ElementInterface;
use BazaarvoiceProductFeed\ProductFeedInterface;
use Drupal\bazaarvoice\Service\BazaarvoiceServiceInterface;
use Drupal\bazaarvoice_products\Service\ProductsServiceInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;

class ProductfeedBuilderService implements ProductfeedBuilderServiceInterface {

  protected $config;
  protected $state;
  protected $database;
  protected $moduleHandler;
  protected $entityManager;
  protected $bazaarvoice;
  protected $bazaarvoice_productfeed;
  protected $bazaarvoice_products;
  protected $productfeed;
  protected $default_file_scheme;


  public function __construct($config, StateInterface $state, Connection $database, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entityTypeManager, BazaarvoiceServiceInterface $bazaarvoice, ProductsServiceInterface $bazaarvoice_products, ProductfeedServiceInterface $bazaarvoice_productfeed, ProductFeedInterface $productfeed) {
    $this->config = $config->get('bazaarvoice.productfeed.settings');
    $this->state = $state;
    $this->database = $database;
    $this->moduleHandler = $module_handler;
    $this->entityManager = $entityTypeManager;
    $this->bazaarvoice = $bazaarvoice;
    $this->bazaarvoice_products = $bazaarvoice_products;
    $this->bazaarvoice_productfeed = $bazaarvoice_productfeed;
    $this->productfeed = $productfeed;

    if ($config->get('bazaarvoice.settings')->get('mode') == 'stg') {
      $this->productfeed->useStage();
    }

  }

  public function buildProduct($entity_id, $entity_type, $bundle_name) {
    $product = [];
    $mapping = $this->bazaarvoice_productfeed->getEntityTypeAttributes($entity_type, $bundle_name);
    $entity = $this->entityManager->getStorage($entity_type)->load($entity_id);
    $translations = $entity->getTranslationLanguages();

    // Attempt to get entity mapping and  load the entity.
    if ($mapping && $entity) {
      // Loop through all mapped attributes.
      foreach ($mapping as $attribute_name => $attribute) {
        $attribute_value = false;
        if ($attribute['field'] && $entity->hasField($attribute['field'])) {
          // Is this attribute translatable and the entity has multiple translations?
          if ($attribute['translatable'] && (count($translations) > 1)) {
            // Loop through each translation.
            foreach ($translations as $translation) {
              // Get translated version of this entity.
              $trans_entity = $entity->getTranslation($translation->getId());
              // Get translated version of the field value.
              if ($trans_entity->hasField($attribute['field']) && ($value = $this->getAttributeEntityFieldValue($trans_entity, $trans_entity->get($attribute['field']), $attribute))) {
                // Add to values array.
                $attribute_value[$this->bazaarvoice->getBazaarvoiceLocaleCode($translation->getId())] = $value;
              }
            }
          }
          elseif ($value = $this->getAttributeEntityFieldValue($entity, $entity->get($attribute['field']), $attribute)) {
            $attribute_value = $value;
          }
        }

        // Have a value?
        if ($attribute_value) {
          // Is an array?
          if (is_array($attribute_value)) {
            foreach ($attribute_value as $key => &$value) {
              $value = $this->formatAttributeValue($attribute_name, $value);
            }
            // attribute support multiple values?
            if ($attribute['multiple']) {
              // Translatable and have translations?
              if ($attribute['translatable'] && (count($translations) > 1)) {
                // Pop-off default language as single.
                $product[$attribute_name] = $attribute_value[$this->bazaarvoice->getBazaarvoiceLocaleCode($entity->language()->getId())];
                unset($attribute_value[$this->bazaarvoice->getBazaarvoiceLocaleCode($entity->language()->getId())]);
                // Set remaining values as multiple.
                if (!empty($attribute_value)) {
                  $product[$attribute['multiple']] = $this->formatAttributeValue($attribute['multiple'], $attribute_value);
                }
              }
              else {
                // Pop off first value.
                $product[$attribute_name] = array_pop($attribute_value);
                // Assign rest to the multiple.
                if (!empty($attribute_value)) {
                  $product[$attribute['multiple']] = $this->formatAttributeValue($attribute['multiple'], $attribute_value);
                }
              }
            }
            // Do not allow multiple get single value.
            else {
              // Translations?
              if ($attribute['translatable'] && (count($translations) > 1)) {
                // Pop-off default language as single.
                $product[$attribute_name] = $attribute_value[$this->bazaarvoice->getBazaarvoiceLocaleCode($entity->language()->getId())];
              }
              // else use first array value
              else {
                $attribute_value = array_shift($attribute_value);
                $product[$attribute_name] = $attribute_value;
              }
            }
          }
          else {
            $product[$attribute_name] = $this->formatAttributeValue($attribute_name, $attribute_value);
          }
        }
      }

      if ($mod_product = $this->moduleHandler->alter('bazaarvoice_productfeed_build_product', $product, $entity)) {
        $product = array_merge_recursive($product, $mod_product);
      }
      $this->saveProduct($entity_id, $entity_type, $bundle_name, $product);
    }
  }

  public function buildFeed() {
    $feed_built = FALSE;
    // @todo: get proper config values for saving the file.
    $site_name = $this->config->get('feed.site_name');

    $feed = $this->productfeed->newFeed($site_name);

    // Get all products.
    if ($products = $this->getProducts()) {
      // Use products to get brand element objects.
      if ($brand_elements = $this->getBrandElementsFromProducts($products)) {
        $feed->addBrands($brand_elements);
      }
      // Use products to get category element objects.
      if ($category_elements = $this->getCategoryElementsFromProducts($products)) {
        $feed->addCategories($category_elements);
      }
      // Use products to get product element objects.
      if ($product_elements = $this->getProductElementsFromProducts($products)) {
        $feed->addProducts($product_elements);
      }
    }

    // Save feed.
    $file_directory = 'bazaarvoice/productfeeds';
    $file_name = $this->config->get('feed.file_name') .  date('Y_m_d');

    if ($productfeed_file = $this->productfeed->saveFeed($feed, $file_directory, $file_name)) {
      // Save productfeed file to state.
      $this->state->set('bazaarvoice_productfeed.feedFile', $productfeed_file);
      $feed_built = TRUE;
    }

    return $feed_built;
  }

  public function sendFeed() {
    $sent_feed = FALSE;
    // Get productfeed file location.
    if ($feed_file = $this->state->get('bazaarvoice_productfeed.feedFile')) {

      // Has time before sending feed again elapsed.
      $feed_frequency = $this->config->get('feed.frequency');
      $last_sent = $this->state->get('bazaarvoice_productfeed.lastSent');

      // Feed never been sent or time has elapsed?
      if ($feed_frequency > 0 && (!$last_sent || (time() - $last_sent) > $feed_frequency)) {

        // Get sFTP values.
        $sftp_username = $this->config->get('sftp.username');
        $sftp_password = $this->config->get('sftp.password');
        $sftp_directory = $this->config->get('sftp.directory');
        $sftp_port = $this->config->get('sftp.port');
        // Attempt to send feed.
        if ($sent_feed = $this->productfeed->sendFeed($feed_file, $sftp_username, $sftp_password, $sftp_directory, $sftp_port)) {
          // Were there errors?
          if ($sent_feed !== TRUE) {
            // log/report error.
          }
          else {
            $this->state->set('bazaarvoice_productfeed.lastSent', time());
          }
        }
      }
    }
    return $sent_feed;
  }

  private function getBrandElementsFromProducts(array $products = []) {
    $brands = [];

    foreach ($products as $product) {
      $product = $product['product_data'];
      // Have brand id and not already proccessed?
      if (isset($product['BrandExternalId']) && !isset($brands[$product['BrandExternalId']])) {
        // Get required fields.
        $external_id = $product['BrandExternalId'];
        $name = isset($product['BrandName']) ? $product['BrandName'] : FALSE;

        // Have required fields?
        if ($external_id && $name) {
          $element = $this->productfeed->newBrand($external_id, $name);
          // Add additional attributes.
          $this->setElementAttributes($element, 'brand', $product);
          $brands[$external_id] = $element;
        }
      }
    }

    return empty($brands) ? FALSE : array_values($brands);
  }

  private function getCategoryElementsFromProducts(array $products = []) {
    $categories = [];

    foreach ($products as $product) {
      $product = $product['product_data'];
      // Have category external id and not already proccessed?
      if (isset($product['CategoryExternalId']) && !isset($categories[$product['CategoryExternalId']])) {
        // Get required attributes for a category.
        $external_id = $product['CategoryExternalId'];
        $name = isset($product['CategoryName']) ? $product['CategoryName'] : FALSE;
        $page_url = isset($product['CategoryPageUrl']) ? $product['CategoryPageUrl'] : FALSE;

        // Have all required fields?
        if ($external_id && $name && $page_url) {
          // Create new element.
          $element = $this->productfeed->newCategory($external_id, $name, $page_url);
          // Add additional attributes.
          $this->setElementAttributes($element, 'category', $product);
          $categories[$external_id] = $element;
        }
      }
    }

    return empty($categories) ? FALSE : array_values($categories);
  }

  private function getProductElementsFromProducts(array $products = []) {
    $elements = [];

    foreach ($products as $product) {
      $product = $product['product_data'];
      if (isset($product['ProductExternalId']) && !isset($elements[$product['ProductExternalId']])) {
        // Get required fields.
        $external_id = $product['ProductExternalId'];
        $name = isset($product['Name']) ? $product['Name'] : FALSE;
        $category_id = isset($product['CategoryExternalId']) ? $product['CategoryExternalId'] : FALSE;
        $page_url = isset($product['PageUrl']) ? $product['PageUrl'] : FALSE;
        $image_url = isset($product['ImageUrl']) ? $product['ImageUrl'] : FALSE;

        // Have all required fields to make a product?
        if ($external_id && $name && $category_id && $page_url && $image_url) {
          // Create new product element.
          $element = $this->productfeed->newProduct($external_id, $name, $category_id, $page_url, $image_url);
          // Add additional attributes to element.
          unset($product['Name'], $product['ProductExternalId'], $product['CategoryExternalId'], $product['PageUrl'], $product['ImageUrl']);
          $this->setElementAttributes($element, 'product', $product);
          $elements[$external_id] = $element;
        }
      }
    }

    return empty($elements) ? FALSE : array_values($elements);
  }

  private function setElementAttributes(ElementInterface &$element, $element_type, array $attributes = []) {

    switch ($element_type) {
      case 'brand':
        // Only additional attribute brand allows is additional names.
        if (isset($attributes['BrandNames'])) {
          foreach ($attributes['BrandNames'] as $locale => $name) {
            $element->addName($name, $locale);
          }
        }
        break;
      case 'category':
        foreach ($attributes as $attribute => $values) {
          switch ($attribute) {
            case 'CategoryNames':
              foreach ($values as $locale => $name) {
                $element->addName($name, $locale);
              }
              break;
            case 'CategoryPageUrls':
              foreach ($values as $locale => $url) {
                $element->addPageUlr($url, $locale);
              }
              break;
            case 'CategoryImageUrl':
              $element->setImageUrl($values);
              break;
            case 'CategoryImageUrls':
              foreach ($values as $locale => $url) {
                $element->addImageUrl($url, $locale);
              }
              break;
            case 'CategoryParentExternalId':
              $element->setParentId($values);
              break;
          }
        }
        break;
      case 'product':
        // Ignore brand and category attributes.
        $ignore_attributes = [
          'CategoryName',
          'CategoryNames',
          'CategoryPageUrl',
          'CategoryPageUrls',
          'CategoryImageUrl',
          'CategoryImageUrls',
          'CategoryExternalId',
          'CategoryParentExternalId',
          'BrandName',
          'BrandNames',
        ];
        foreach ($attributes as $attribute => $values) {
          if ($values) {
            switch ($attribute) {
              case 'Names':
                foreach ($values as $locale => $name) {
                  $element->addName($name, $locale);
                }
                break;
              case 'ProductPageUrls':
                foreach ($values as $locale => $url) {
                  $element->addPageUrl($url, $locale);
                }
                break;
              case 'Description':
                if (is_string($values)) {
                  $element->setDescription($values);
                }
                break;
              case 'Descriptions':
                foreach ($values as $locale => $description) {
                  if (is_string($description)) {
                    $element->addDescription($description, $locale);
                  }
                }
                break;
              case 'ImageUrls':
                foreach ($values as $locale => $url) {
                  $element->addImageUrl($url, $locale);
                }
                break;
              case 'BrandExternalId':
                $element->setBrandId($values);
                break;
              case 'ModelNumber':
              case 'ModelNumbers':
                if (!is_array($values)) {
                  $values = [$values];
                }
                foreach ($values as $model_number) {
                  $element->addModelNumber($model_number);
                }
                break;
              case 'ManufacturerPartNumber':
              case 'ManufacturerPartNumbers':
                if (!is_array($values)) {
                  $values = [$values];
                }
                foreach ($values as $part_number) {
                  $element->addManufacturerPartNumber($part_number);
                }
                break;
              case 'UPC':
              case 'UPCs':
                if (!is_array($values)) {
                  $values = [$values];
                }
                foreach ($values as $upc) {
                  $element->addUPC($upc);
                }
                break;
              case 'EAN':
              case 'EANs':
                if (!is_array($values)) {
                  $values = [$values];
                }
                foreach ($values as $ean) {
                  $element->addEAN($ean);
                }
                break;
              case 'ISBN':
              case 'ISBNs':
                if (!is_array($values)) {
                  $values = [$values];
                }
                foreach ($values as $isbn) {
                  $element->addISBN($isbn);
                }
                break;
              default:
                // If not an ignored attribute and is a single value.
                if (!in_array($attribute, $ignore_attributes) && is_string($values)) {
                  // Set as a custom attribute.
                  $element->addCustomAttribute($attribute, $values);
                  break;
                }
                break;
            }
          }
        }
        break;
      default:
        // Allow other modules to set via an alter hook.
        $this->moduleHandler->alter('bazaarvoice_productfeed_element_attribute', $element, $element_type, $attributes);
        break;
    }
  }

  private function getAttributeEntityFieldValue($entity, $field, array $attribute = []) {
    $field_value = FALSE;

    $field_type = $field->getFieldDefinition()->getType();
    // Get value based on field type.
    switch ($field_type) {
      case 'path':
        $field_value = $entity->toUrl()->setAbsolute(true)->toString();
        break;
      case 'image':
        $field_value = file_create_url($field->entity->getFileUri());
        break;
      case 'integer':
      case 'string':
      case 'string_long':
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        $field_value = $field->getValue();
        if (is_array($field_value)) {
          foreach ($field_value as $v => &$value) {
            $value = isset($value['value']) ? $value['value'] : array_shift($value);
            if (!$value) {
              unset($field_value[$v]);
            }
          }
        }
        if (is_array($field_value) && count($field_value) == 1) {
          $field_value = array_pop($field_value);
        }
        break;
      case 'uuid':
        $field_value = $entity->uuid();
        break;
      case 'entity_reference':
        if ($referenced_entities = $field->referencedEntities()) {
          if ($attribute['multiple']) {
            foreach ($referenced_entities as $sub_entity) {
              if ($attribute['sub_field'] && $sub_entity->hasField($attribute['sub_field'])) {
                if ($sub_value = $this->getAttributeEntityFieldValue($sub_entity, $sub_entity->get($attribute['sub_field']), $attribute)) {
                  $field_value[] = $sub_value;
                }
              }
              else {
                $field_value[] = $sub_entity->id();
              }
            }
          }
          else {
            $sub_entity = array_shift($referenced_entities);
            if ($attribute['sub_field'] && $sub_entity->hasField($attribute['sub_field'])) {
              if ($sub_value = $this->getAttributeEntityFieldValue($sub_entity, $sub_entity->get($attribute['sub_field']), $attribute)) {
                $field_value = $sub_value;
              }
            }
            else {
              $field_value = $sub_entity->id();
            }
          }
        }
        break;
      default:
        $config = [
          'entity' => $entity,
          'field' => $field,
          'attribute' => $attribute,
        ];
        $field_value = $this->moduleHandler->invokeAll('bazaarvoice_productfeed_field_value', $config);
        break;
    }

    return $field_value;
  }

  private function formatAttributeValue($attribute_name, $attribute_value) {
    switch ($attribute_name) {
      case 'ProductExternalId':
      case 'BrandExternalId':
      case 'CategoryExternalId':
      case 'CategoryParentExternalId':
        // format into a proper id.
        $attribute_value = $this->bazaarvoice_products->formatProductId($attribute_value);
        break;
      default:
        // Allow other modules to format.
        $this->moduleHandler->alter('bazaarvoice_productfeed_format_attribute_value', $attribute_value, $attribute_name);
        break;
    }
    return $attribute_value;
  }

  private function saveProduct($entity_id, $entity_type, $bundle_name, $product_data) {
    $this->database->merge('bazaarvoice_productfeed_entities')
      ->key([
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'entity_bundle' => $bundle_name
      ])
      ->fields([
        'product_data' => serialize($product_data),
      ])
      ->execute();
  }

  private function getProducts($entity_type = FALSE, $bundle_name = FALSE, $entity_id = FALSE) {
    $products = [];

    $query = $this->database->select('bazaarvoice_productfeed_entities', 'bpe')
      ->fields('bpe');

    if ($entity_type) {
      $query->condition('entity_type', $entity_type);
    }

    if ($bundle_name) {
      $query->condition('entity_bundle', $bundle_name);
    }

    if ($entity_id) {
      $query->condition('entity_id', $entity_id);
    }

    if ($product_data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
      foreach ($product_data as &$product) {
        $product['product_data'] = unserialize($product['product_data']);
        $products[] = $product;
      }
    }

    return $products;
  }

}