<?php

namespace Drupal\bazaarvoice_productfeed\Service;

use Drupal\bazaarvoice_products\Service\ProductsServiceInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ProductfeedService implements ProductfeedServiceInterface {

  use UseCacheBackendTrait;
  use StringTranslationTrait;

  protected $config;
  protected $database;
  protected $moduleHandler;
  protected $entityTypeManager;
  protected $fieldManager;
  protected $queue;
  protected $products;
  protected $attributes;
  protected $custom_attributes;

public function __construct($config, Connection $database, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager, QueueFactory $queue, ProductsServiceInterface $products) {

    $this->config = $config->get('bazaarvoice.productfeed.settings');
    $this->database = $database;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->queue = $queue;
    $this->products = $products;
  }

  public function getEntityTypeFormFields($entity_type, $bundle_name, array $values = []) {
    $form_fields = [];

    // Get attribute maps for entity type fields.
    if ($attributes_map = $this->getEntityTypeAttributes($entity_type, $bundle_name)) {
      // Update with submitted values if present.
      if (isset($values['attributes_mapping'])) {
        // Merge the submitted values into the map.
        foreach ($values['attributes_mapping'] as $grouping => $groups) {
          foreach ($groups as $group => $submitted_attributes) {
            $attributes_map = array_replace_recursive($attributes_map, $submitted_attributes);
          }
        }
      }

      // Loop through each mapping to build form fields.
      foreach ($attributes_map as $attribute => $mapping) {
        $group = isset($mapping['group']) ? $mapping['group'] : 'Other';
        $container = $group . '_container';

        if (!isset($form_fields[$container])) {
          $form_fields[$container] = [
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => $this->t('@group Attributes', ['@group' => $group]),
          ];
          $form_fields[$container][$group] = [
            '#type' => 'table',
            '#header' => [
              $this->t('Attribute'),
              $this->t('Field'),
              $this->t('SubField'),
            ]
          ];
        }

        $classes = 'bazaarvoice_' . strtolower($attribute);
        if (isset($mapping['required']) && $mapping['required']) {
          $classes .=' form-required';
        }

        $form_fields[$container][$group][$attribute]['attribute'] = [
          '#markup' => $mapping['label'],
          '#prefix' => '<div class="' . $classes . '">',
          '#suffix' => '</div>',
        ];

        if (isset($mapping['required']) && $mapping['required']) {
          $form_fields[$container][$group][$attribute]['attribute']['#attributes']['class'][] = 'form-required';
        }

        // Build default options array.
        $options = [];//'' => $this->t('Select Field')];
        $empty_option = $this->t('Select Field');
        // Available fields to select?
        if (isset($mapping['field_options'])) {
          $options = $mapping['field_options'];
        }
        // No fields to select, reset the options array.
        else {
          $empty_option = $this->t('No Fields Available');
          //$options = ['' => t('No Fields Available')];
        }

        // Primary field mapping.
        $form_fields[$container][$group][$attribute]['field'] = [
          '#type' => 'select',
          '#options' => $options,
          '#empty_option' => $empty_option,
          '#default_value' => isset($mapping['field']) ? $mapping['field'] : '',
          '#disabled' => (count($options) == 1),
        ];

        if (isset($mapping['required']) && $mapping['required']) {
          $form_fields[$container][$group][$attribute]['field']['#states']['required'] = [
            ':input[name=bazaarvoice_product_type]' => [
              'checked' => TRUE,
              ],
          ];
        }

        // Sub field mapping.
        $form_fields[$container][$group][$attribute]['sub_field'] = [
          '#prefix' => '<div id="bazaarvoice_' . strtolower($attribute) . '_sub">',
          '#suffix' => '</div>',
        ];

        // Supports subfield?
        if ($this->attributeSupportsSubField($mapping)) {
          // Build wrapper for dynamic field selection for sub field.
          $form_fields[$container][$group][$attribute]['field']['#ajax'] = [
            'callback' => 'bazaarvoice_productfeed_attribute_subfield_callback',
            'wrapper' => 'bazaarvoice_' . strtolower($attribute) . '_sub',
            'method' => 'replace',
          ];
          // if field is not blank add info for sub field.
          if ($mapping['field']) {
            // Get sub field options.
            $form_fields[$container][$group][$attribute]['sub_field'] += $this->getEntityTypeFormSubFields($attribute, $mapping['field'], $entity_type, $bundle_name, $mapping);
          }
        }
      }
    }

    return $form_fields;
  }

  public function validateEntityTypeFormFields($entity_type, $bundle_name, FormStateInterface $form_state) {
    $values = $form_state->getValues();


    // Get attribute maps for entity type fields.
    if ($attributes_map = $this->getEntityTypeAttributes($entity_type, $bundle_name)) {
      // Update with submitted values if present.
      if (isset($values['attributes_mapping'])) {
        // Merge the submitted values into the map.
        foreach ($values['attributes_mapping'] as $grouping => $groups) {
          foreach ($groups as $group => $submitted_attributes) {
            $attributes_map = array_replace_recursive($attributes_map, $submitted_attributes);
          }
        }
      }

      // Loop through each mapping to build form fields.
      foreach ($attributes_map as $attribute => $mapping) {
        $group = isset($mapping['group']) ? $mapping['group'] : 'Other';
        $container = $group . '_container';

        if (isset($mapping['required']) && !$mapping['field']) {
          $form_state->setErrorByName('attributes_mapping][' . $container . '][' . $group .'][' . $attribute .'][field', $this->t('@group @attribute attribute mapping is required.', [
            '@group' => $group,
            '@attribute' => $mapping['label'],
          ]));
        }
      }
    }
  }

  /**
   * Build Drupal form fields for sub field elements.
   *
   * @param string $attribute_name
   *   Name of attribute.
   * @param string $field_name
   *   Name of field assigned to attribute.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle_name
   *   Entity bundle name
   * @param array $attribute_data
   *   (optional) Array of attribute mapping data.
   * @return array
   */
  private function getEntityTypeFormSubFields($attribute_name, $field_name, $entity_type, $bundle_name, array $attribute_data = []) {
    // Defautl blank option.
    $empty_option = $this->t('No Fields Available');
    $options = [];//'' => $this->t('No Fields Available')];

    // Field exist in attribute data?
    if ($attribute_data['fields'] && isset($attribute_data['fields'][$field_name])) {
      // Build a temp attribute.
      $temp_attribute = [
        'field_types' => $attribute_data['field_types'],
      ];

      // Check if passed field types accept sub fields.
      if ($this->attributeSupportsSubField($temp_attribute)) {
        // Get sub fields.
        if ($sub_fields = $this->getEntityAttributeSubFields($attribute_name, $entity_type, $bundle_name, $field_name, $attribute_data)) {
          // Build options array.
          $empty_option = $this->t('Select Field');
          $options = $sub_fields;// array_merge(['' => $this->t('Select Field')], $sub_fields);
        }
      }
    }
    // Build form field.
    $form_field = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => $empty_option,
      '#default_value' => isset($attribute_data['sub_field']) ? $attribute_data['sub_field'] : '',
      '#disabled' => (count($options) == 1),
    ];

    return $form_field;
  }


  public function attributeSupportsSubField($attribute) {
    $supports = FALSE;
    // Entity reference?
    if (in_array('entity_reference', $attribute['field_types'])
    ) {
      $supports = TRUE;
    }
    // Allow other modules to provide info for this attributes.
    else {
      $module_support = $this->moduleHandler->invokeAll('bazaarvoice_productfeed_supports_subfield', [$attribute]);
      // If returned an array and sum is greater then 0, then supports subfields.
      if (is_array($module_support)) {
        $supports = (array_sum($module_support) > 0);
      }
    }

    return $supports;
  }

  /**
   * Return a list of Sub Fields for an attribute.
   *
   * @param string $attribute_name
   *   Name of the attribute.
   * @param string $entity_type
   *   Entity type
   * @param string $bundle_name
   *   Entity bundle.
   * @param string $field_name
   *   Name of parent field.
   * @param array $attribute_data
   *   (optional) array of attribute mapping data.
   *
   * @return array
   *   Array of available sub fields.
   */
  private function getEntityAttributeSubFields($attribute_name, $entity_type, $bundle_name, $field_name, array $attribute_data = []) {
    $sub_fields = array();
    // Only care about the field types.
    $attribute_data = array(
      'field_types' => $attribute_data['field_types'],
    );
    // Have sub field reference the same field types, minus any enttiy references.
    unset($attribute_data['field_types'][array_search('entity_reference', $attribute_data['field_types'])]);
    // Get field info.
    $entity_fields = $this->fieldManager->getFieldDefinitions($entity_type, $bundle_name);

    if (isset($entity_fields[$field_name])) {
      $field = $entity_fields[$field_name];

      // Switch based on field type.
      switch ($field->getType()) {

        case 'entity_reference':
          // Loop through each bundle available for referencing.
          foreach ($field->getSetting('handler_settings')['target_bundles'] as $bundle) {
            $attributes = $this->getEntityTypeAttributes($field->getSetting('target_type'), $bundle);
            if (isset($attributes[$attribute_name]['field_options'])) {
              $sub_fields = array_merge($sub_fields, $attributes[$attribute_name]['field_options']);
            }
          }
          break;
        default:
          // Allow other modules to dictate sub fields.
          $sub_fields = $this->moduleHandler->invokeAll('bazaarvoice_productfeed_entity_attribute_subfields', [
            $attribute_name,
            $entity_type,
            $field_name,
            $attribute_data
          ]);
          break;
      }

      // Allow other modules to alter
      $context = [$attribute_name, $entity_type, $field_name, $attribute_data];
      $this->moduleHandler->alter('bazaarvoice_productfeed_entity_attribute_subfields', $sub_fields, $context);
    }

    return $sub_fields;
  }

  public function getEntityTypeAttributes($entity_type, $bundle_name) {
    $attributes_map = [];

    // Get entity fields.
    if (($entity_info = $this->entityTypeManager->getDefinition($entity_type)) && ($fields = $this->fieldManager->getFieldDefinitions($entity_type, $bundle_name))) {
      // Get Bazaarvoice attributes.
      $attributes = $this->getAttributes();
      // Get Entity map.
      if ($entity_map = $this->getEntityTypeMap($entity_type, $bundle_name, array_keys($attributes))) {
        // Merge entity map into attributes array.
        $attributes = array_merge_recursive($attributes, $entity_map);
      }

      // Loop through all of the attributes.
      foreach ($attributes as $attribute_name => $attribute) {
        // Attribute not have field types? then move on.
        if (!isset($attribute['field_types'])) {
          continue;
        }

        $attributes_map[$attribute_name] = $attribute;

        // If this attribute supports integers and has an id field.
        if (in_array('integer', $attribute['field_types']) && ($id = $entity_info->getKey('id'))) {
          // Add the id as a field option.
          $attributes_map[$attribute_name]['field_options'][$id] = $this->t('@entity_label ID (@id)', [
            '@entity_label' => $entity_info->get('label'),
            '@id' => $id,
          ]);
        }

        // If this attribute supports text.
        if (in_array('text', $attribute['field_types'])) {
          //this entity has a label?
          if ($label = $entity_info->getKey('label')) {
            // Add the label as a field option.
            $attributes_map[$attribute_name]['field_options'][$label] = $this->t('@label (label)', array('@label' => ucwords($label)));
          }

          // This entity supports UUID?
          if ($uuid = $entity_info->getKey('uuid')) {
            $attributes_map[$attribute_name]['field_options'][$uuid] = $this->t('Universal Unique Identifier (uuid)');
          }

          // This entity supports having a canonical Url?

          if ($entity_info->getLinkTemplate('canonical')) {
            $attributes_map[$attribute_name]['field_options']['path'] = $this->t('Entity Url');
          }
        }

        if (!empty($fields)) {
          // Loop through the entity fields.
          foreach ($fields as $field_name => $field) {
            //$field_type = $field->getDataType();
            $field_type = $field->getType();
            // This field supported by this attribute?
            if (in_array($field_type, $attribute['field_types'])) {
              // Add this field as an option for this attribute.
              $attributes_map[$attribute_name]['fields'][$field_name] = $field_type;
              $attributes_map[$attribute_name]['field_options'][$field_name] = $field->getLabel();
            }
          }
        }
      }
    }

    // Allow other modules to alter.
    $this->moduleHandler->alter('bazaarvoice_productfeed_entity_attributes', $attributes_map, $entity_type, $bundle_name);

    return $attributes_map;

  }

  private function getEntityTypeProductfeedAttributes($entity_type, $bundle_name) {
    $attributes = [];
    if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
      $attributes = $entity->getThirdPartySetting('bazaarvoice_productfeed', 'attributes_map', []);
    }
    return $attributes;
  }

  private function setEntityTypeProductfeedAttributes($entity_type, $bundle_name, array $attributes = []) {
    if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
      $entity->setThirdPartySetting('bazaarvoice_productfeed', 'attributes_map', $attributes);
      $entity->save();
    }
  }

  public function getEntityTypeMap($entity_type, $bundle_name, array $attribute_names = []) {
    $attributes_map = [];

    $entity_attributes = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle_name);

    // Add each to mapping to attributes map.
    foreach ($entity_attributes as $entity_attribute) {
      $attributes_map[$entity_attribute['attribute']] = [
        'field' => $entity_attribute['field_name'],
        'sub_field' => $entity_attribute['sub_field_name'],
      ];
      // remove from array of retrieved attributes.
      unset($attribute_names[array_search($entity_attribute['attribute'], $attribute_names)]);
    }

    // Any attributes left over?
    if (!empty($attribute_names)) {
      // Build empty mappings.
      foreach ($attribute_names as $attribute) {
        $attributes_map[$attribute] = [
          'field' => '',
          'sub_field' => '',
        ];
      }
    }

    // Allow other modules to add mappings.
    if ($module_attributes = $this->moduleHandler->invokeAll('bazaarvoice_productfeed_entity_mappings', [
      'entity_type' => $entity_type,
      'bundle_name' => $bundle_name,
    ])) {
      $attributes_map = array_merge_recursive($attributes_map, $module_attributes);
    }

    // Allow other modules to alter mappings.
    $context = [
      'entity_type' => $entity_type,
      'bundle_name' => $bundle_name,
    ];
    $this->moduleHandler->alter('bazaarvoice_productfeed_entity_mappings', $attributes_map, $context);

    // Return attributes map.
    return $attributes_map;
  }

  public function saveEntityTypeMap($entity_type, $bundle_name, $attributes_map) {
    $new_map = [];
    // Get the old map
    $old_map = $this->getEntityTypeMap($entity_type, $bundle_name);

    // get list of attributes that have been removed.
    $removed_attributes = array_diff(array_keys($old_map), array_keys($attributes_map));

    // Loop through each mapping.
    foreach ($attributes_map as $attribute_name => $data) {
      if (in_array($attribute_name, $removed_attributes)) {
        continue;
      }
      else {
        $new_map[$attribute_name] = [
          'attribute' => $attribute_name,
          'field_name' => $data['field'],
          'sub_field_name' => isset($data['sub_field']) ? $data['sub_field'] : ''
        ];
      }
    }

    $this->setEntityTypeProductfeedAttributes($entity_type, $bundle_name, $new_map);

    $this->queueProductFeedEntityRebuild($entity_type, $bundle_name);

    // Let other modules know.
    $this->moduleHandler->invokeAll('bazaarvoice_productfeed_save_entity_attributes', [
        $entity_type,
        $bundle_name,
        $attributes_map,
      ]);
  }

  public function deleteEntityTypeMap($entity_type, $bundle_name = FALSE) {

    if ($bundle_name) {
      if ($product_type = $this->getEntityTypeObject($entity_type, $bundle_name)) {
        $product_type->unsetThirdPartySetting('bazaarvoice_productfeed', 'attributes_map');
        $product_type->save();
      }
    } elseif ($product_types = $this->products->getProductTypes($entity_type)) {
      foreach ($product_types[$entity_type] as $bundle => $data) {
        if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
          $entity->unsetThirdPartySetting('bazaarvoice_productfeed', 'attributes_map');
          $entity->save();
        }
      }
    }

    $entity_query = $this->database->delete('bazaarvoice_producfeed_entities')
      ->condition('entity_type', $entity_type);
    // Deleting only a specific bundle?
    if ($bundle_name) {
      // Add bundle condition.
      $entity_query->condition('bundle_name', $bundle_name);
    }

    $entity_query->execute();

    // Let other modules know.
    $this->moduleHandler->invokeAll('bazaarvoice_productfeed_delete_entity_attributes', [
        $entity_type,
        $bundle_name,
      ]);

  }

  public function getAttributes() {
    // Get default attributes.
    $attributes = $this->getDefaultAttributes();
    // Get any attributes defined by other modules.
    if ($module_attributes = $this->moduleHandler->invokeAll('bazaarvoice_productfeed_attributes')) {
      $attributes = array_merge($attributes, $module_attributes);
    }

    // Provide attribute name as an element for each attribute.
    foreach ($attributes as $name => &$data) {
      $data['attribute'] = $name;
    }

    // Get any custom attributes stored in the DB.
    if ($custom_attributes = $this->getCustomAttributes()) {
      $attributes = array_merge($attributes, $custom_attributes);
    }

    // Allow other modules to alter.
    $this->moduleHandler->alter('bazaarvoice_productfeed_attributes', $attributes);


    return $attributes;
  }

  public function getCustomAttributes() {
    $custom_attributes = [];
    //@TODO: make into config entity.
    /*
    $attributes = $this->database->select('bazaarvoice_productfeed_custom_attributes', 'bvca')
      ->fields('bvca')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    if ($attributes) {
      // Unserialize field types.
      foreach ($attributes as &$attribute) {
        $attribute['group'] = 'Custom';
        $attribute['field_types'] = unserialize($attribute['field_types']);
      }
      $custom_attributes = $attributes;
    }
*/
    // Allow other modules to alter.
    $this->moduleHandler->alter('bazaarvoice_productfeed_custom_attributes', $custom_attributes);

    return $custom_attributes;
  }

  public function saveCustomAttributes(array $custom_attributes) {

    //@todo: make into config entities.
    /*
    $attribute_names = [];
    foreach ($custom_attributes as $attribute) {

      $this->database->merge('bazaarvoice_productfeed_custom_attributes')
        ->key([
          'attribute' => $custom_attributes['attribute']
        ])
        ->fields([
          'label' => $attribute['label'],
          'field_types' => serialize($attribute['field_types']),
        ])
        ->execute();
      $attribute_names[] = $custom_attributes['attribute'];
    }

    if ($product_types = $this->products->getProductTypes()) {
      foreach ($product_types as $entity_type => $bundles) {
        foreach ($bundles as $bundle => $data) {
          if ($mapping = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle)) {
            $mapping_attributes = array_keys($mapping);
            $intersect = array_intersect($attribute_names, $mapping_attributes);
            if (!empty($intersect)) {
              $this->queueProductFeedEntityRebuild($entity_type, $bundle);
            }
          }
        }
      }
    }
*/
    // Allow other modules to alter.
    $this->moduleHandler->invokeAll('bazaarvoice_productfeed_save_custom_attributes', $custom_attributes);
  }

  public function deleteCustomAttributes(array $custom_attributes) {
    // @todo: make into config entities.
    /*
    $this->database->delete('bazaarvoice_productfeed_custom_attributes')
      ->condition('attribute', $custom_attributes, 'IN')
      ->execute();

    if ($product_types = $this->products->getProductTypes()) {
      foreach ($product_types as $entity_type => $bundles) {
        foreach ($bundles as $bundle => $data) {
          if ($mapping = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle)) {
            $mapping_attributes = array_keys($mapping);
            $intersect = array_intersect($custom_attributes, $mapping_attributes);
            if (!empty($intersect)) {
              $this->queueProductFeedEntityRebuild($entity_type, $bundle);
              foreach ($intersect as $remove_attribute) {
                unset($mapping[$remove_attribute]);
              }
              $this->setEntityTypeProductfeedAttributes($entity_type, $bundle, $mapping);
            }
          }
        }
      }
    }
*/
    // Allow other modules to alter.
    $this->moduleHandler->invokeAll('bazaarvoice_productfeed_delete_custom_attributes', $custom_attributes);

  }

  public function queueProductFeedRebuild(array $queue_actions = []) {

    $this->queue->get('bazaarvoice_productfeed_queue')->createItem($queue_actions);
  }

  public function getEntityTypeFieldAttribute($entity_type, $bundle_name, $field_name) {
    $field_attribute = FALSE;

    if ($entity_map = $this->getEntityTypeMap($entity_type, $bundle_name)) {
      // Loop through all of the mappings.
      foreach ($entity_map as $attribute => $fields) {
        // Is this field mapped to attribute field?
        if ($fields['field'] == $field_name) {
          $field_attribute = $attribute;
          break;
        }
      }
    }

    return $field_attribute;
  }

  public function queueProductFeedEntityRebuild($entity_type = false, $bundle_name = false, $entity_id = false) {

    $entity_types = [];
    $entity_actions = [];

    // Have a specific entity type?
    if ($entity_type) {

      $entity_types[$entity_type] = [];

      // Have a specific bundle name?
      if ($bundle_name) {
        $entity_types[$entity_type][] = $bundle_name;
      }
      else {
        if ($bundles = $this->products->getProductTypes($entity_type)) {
          $entity_types[$entity_type] = array_keys($bundles[$entity_type]);
        }
      }
    }
    else {
      // Get all entity types that are product types.
      if ($product_entity_types = $this->products->getProductTypes()) {
        foreach ($product_entity_types as $entity_type => $bundles) {
          $entity_types[$entity_type] = array_keys($bundles);
        }
      }
    }

    foreach ($entity_types as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {

        $entity_storage = $this->entityTypeManager->getStorage($entity_type);
        $query = $entity_storage->getQuery();
        $query->accessCheck(FALSE)
          ->condition('type', $bundle);

        // Have specific entity ids?
        if ($entity_id) {
          $id_field = $entity_storage->getEntityType()->getKey('id');
          $query->condition($entity_storage->getEntityType()->getKey('id'), $entity_id);
        }

        if ($entity_ids = $query->execute()) {
          foreach ($entity_ids as $entity_id) {
            $entity_actions[] = [
                'action' => 'build_entity',
                'data' => [
                  'entity_type' => $entity_type,
                  'bundle_name' => $bundle_name,
                  'entity_id' => $entity_id,
                ]
            ];
          }
        }
      }
    }

    if ($entity_actions) {
      // Split into groups of 20.
      $chunked_actions = array_chunk($entity_actions, 20);

      foreach ($chunked_actions as $actions) {
        $this->queueProductFeedRebuild($actions);
      }

      // Dequeue any previously set generate and send commands.
      $this->queueRemoveActions(['generate_productfeed', 'send_productfeed']);

      // Action to resend the feed.
      $queue_actions = [
        [
          'action' => 'generate_productfeed',
        ],
        [
          'action' => 'send_productfeed',
        ]
      ];
      $this->queueProductFeedRebuild($queue_actions);
    }
  }

  private function queueRemoveActions(array $actions = []) {
    //@TODO: this is not the way to accomplish this, find proper way.
    $query = $this->database->delete('queue')
      ->condition('name', 'bazaarvoice_productfeed_queue');

    if (!empty($actions)) {
      $condition_group = $query->orConditionGroup();
      foreach ($actions as $action) {
        $condition_group->condition('data', '%' . $this->database->escapeLike($action) . '%', 'LIKE');
      }
      $query->condition($condition_group);
    }

    $query->execute();
  }

  public function deleteAttributeField($field_name) {

    if ($product_types = $this->products->getProductTypes()) {
      foreach ($product_types as $entity_type => $bundles) {
        foreach ($bundles as $bundle => $data) {
          if ($mapping = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle)) {
            $altered = false;
            foreach ($mapping as $attribute => &$map) {
              if ($map['field_name'] == $field_name) {
                $map['field_name'] = '';
                $map['sub_field_name'] = '';
                $altered = TRUE;
              }
            }

            if ($altered) {
              $this->setEntityTypeProductfeedAttributes($entity_type, $bundle, $mapping);
              $this->queueProductFeedEntityRebuild($entity_type, $bundle);
            }
          }
        }
      }
    }

  }

  public function updateEntityTypeBundle($entity_type, $old_bundle_name, $new_bundle_name) {
    if ($this->products->isProductType($entity_type, $bundle_old)) {
      $mapping = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle_old);
      $this->setEntityTypeProductfeedAttributes($entity_type, $new_bundle_name, $mapping);
      // Update productfeed data.
      $this->database->update('bazaarvoice_producfeed_entities')
        ->fields(['entity_bundle' => $new_bundle_name])
        ->condition('entity_type', $entity_type)
        ->condition('entity_bundle', $old_bundle_name)
        ->execute();
    }
  }

  public function deleteEntityTypeBundle($entity_type, $bundle_name) {
    // Check if this bundle is an product type.
    if ($this->products->isProductType($entity_type, $bundle_name)) {
      if ($entity = $this->getEntityTypeObject($entity_type, $bundle_name)) {
        $entity->unsetThirdPartySetting('bazaarvoice_productfeed', 'attributes_map');
        $entity->save();
      }
      // Delete from feed data table.
      $this->database->delete('bazaarvoice_producfeed_entities')
        ->condition('entity_type', $entity_type)
        ->condition('entity_bundle', $bundle_name)
        ->execute();
    }
  }

  public function deleteEntityTypeAttributeField($entity_type, $bundle_name, $field_name) {

    if ($mapping = $this->getEntityTypeProductfeedAttributes($entity_type, $bundle_name)) {
      $altered = false;
      foreach ($mapping as $attribute => &$map) {
        if ($map['field_name'] == $field_name) {
          $map['field_name'] = '';
          $map['sub_field_name'] = '';
          $altered = TRUE;
        }
      }

      if ($altered) {
        $this->setEntityTypeProductfeedAttributes($entity_type, $bundle, $mapping);
        $this->queueProductFeedEntityRebuild($entity_type, $bundle);
      }
    }
  }

  private function getEntityTypeObject($entity_type, $bundle_name) {
    $entity = FALSE;
    if ($type_storage = $this->entityTypeManager->getStorage($entity_type)->getEntityType()->getBundleEntityType()) {
      $entity = $this->entityTypeManager->getStorage($type_storage)->load($bundle_name);
    }
    return $entity;
  }

  private function getDefaultAttributes() {
    //@TODO: make these into plugins?
    $attributes = [];
    $attributes['Name'] = [
      'label' => $this->t('Name'),
      'multiple' => 'Names',
      'group' => 'Product',
      'translatable' => TRUE,
      'required' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];

    $attributes['PageUrl'] = [
      'label' => $this->t('Page Url'),
      'multiple' => 'PageUrls',
      'group'=> 'Product',
      'translatable' => TRUE,
      'required' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['Description'] = [
      'label' => $this->t('Description'),
      'multiple' => 'Descriptions',
      'group'=> 'Product',
      'translatable' => TRUE,
      'field_types' => [
        'string',
        'string_long',
        'text',
        'text_long',
        'text_with_summary',
        'entity_reference',
      ],
    ];
    
    $attributes['ImageUrl'] = [
      'label' => $this->t('Image Url'),
      'group'=> 'Product',
      'multiple' => 'ImageUrls',
      'translatable' => TRUE,
      'required' => TRUE,
      'field_types' => [
        'image',
        'entity_reference',
      ],
    ];
    
    $attributes['CategoryName'] = [
      'label' => $this->t('Name'),
      'multiple' => 'CategoryNames',
      'group'=> 'Category',
      'translatable' => TRUE,
      'required' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['CategoryExternalId'] = [
      'label' => $this->t('External ID'),
      'group'=> 'Category',
      'required' => TRUE,
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];

    $attributes['CategoryPageUrl'] = [
      'label' => $this->t('Page Url'),
      'group'=> 'Category',
      'multiple' => 'CategoryPageUrls',
      'translatable' => TRUE,
      'required' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];

    $attributes['CategoryImageUrl'] = [
      'label' => $this->t('Image Url'),
      'multiple' => 'CategoryImageUrls',
      'group'=> 'Category',
      'translatable' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];

    $attributes['CategoryParentExternalId'] = [
      'label' => $this->t('Parent External Id'),
      'group'=> 'Category',
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['BrandName'] = [
      'label' => $this->t('Name'),
      'multiple' => 'BrandNames',
      'group'=> 'Brand',
      'translatable' => TRUE,
      'field_types' => [
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['BrandExternalId'] = [
      'label' => $this->t('External ID'),
      'group'=> 'Brand',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['ModelNumber'] = [
      'label' => $this->t('Model Number'),
      'multiple' => 'ModelNumbers',
      'group'=> 'Product',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['ManufacturerPartNumber'] = [
      'label' => $this->t('Manufacturer Part Number'),
      'multiple' => 'ManufacturerPartNumbers',
      'group'=> 'Product',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    $attributes['UPC'] = [
      'label' => $this->t('UPC'),
      'multiple' => 'UPCs',
      'group'=> 'Product',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];

    $attributes['EAN'] = [
      'label' => $this->t('EAN'),
      'multiple' => 'EANs',
      'group'=> 'Product',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];
    $attributes['ISBN'] = [
      'label' => $this->t('ISBN'),
      'multiple' => 'ISBNs',
      'group'=> 'Product',
      'field_types' => [
        'integer',
        'string',
        'text',
        'entity_reference',
      ],
    ];
    
    return $attributes;
  }

}