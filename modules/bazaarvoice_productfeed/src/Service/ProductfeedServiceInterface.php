<?php

namespace Drupal\bazaarvoice_productfeed\Service;

interface ProductfeedServiceInterface {

  /**
   * Return render array of Drupal form fields to map bazaarvoice product attributes
   * to entity type fields.
   *
   * @param string $entity_type
   *   Entity type name
   * @param string $bundle_name
   *   entity bundle name
   *
   * @param array $values
   *  (optional) array of previously submitted values.
   *
   * @return mixed
   */
  public function getEntityTypeFormFields($entity_type, $bundle_name, array $values = []);

  /**
   * Boolean check if attribute supports sub fields.
   *
   * @param string $attribute
   *   Attribute name.
   *
   * @return bool
   *   Boolean TRUE or FALSE.
   */
  public function attributeSupportsSubField($attribute);


  /**
   * Return array of Bazaarvoice product attributes and their field mappings for
   * an entity/bundle type.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle_name
   *   Entity bundle name.
   *
   * @return mixed
   *   Array of Bazaarvoice product attributes mapped to entity fields.
   */
  public function getEntityTypeAttributes($entity_type, $bundle_name);


  /**
   * Return basic array of Bazaarovice Product attributes and field mappings for
   * entity type/bundle.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle_name
   *   Entity bundle name.
   * @param array $attribute_names
   *   (optional) Array of attributes to retrieve.
   *
   * @return mixed
   *   Associative array of Bazaarvoice attributes (keyed by attribute name) and
   * fields that it is mapped to.
   */
  public function getEntityTypeMap($entity_type, $bundle_name, array $attribute_names = []);

  /**
   * Save Entity Type field mappings to Bazaarvoice product feed attributes.
   *
   * @param string $entity_type
   *   Entity type name.
   * @param string $bundle_name
   *   Entity bundle name.
   * @param array $attributes_map
   *   Associative array of Bazaarvoice Product attributes and field mappings.
   *
   * @return mixed
   */
  public function saveEntityTypeMap($entity_type, $bundle_name, $attributes_map);

  /**
   *  Delete Entity type field mappings.
   *
   * @param string $entity_type
   *   entity type name.
   * @param string|bool $bundle_name
   *   (optional) Entity bundle name
   *
   * @return mixed
   */
  public function deleteEntityTypeMap($entity_type, $bundle_name = FALSE);

  /**
   * Return associative array of Bazaarvoice product attributes.
   *
   * @return mixed
   *   Associative array of attributes keyed off of attribute name.
   *  AttributeName =>
   *    label (string) : Human display label of attribute
   *    multiple (string) : Name of attribute if it accepts multiple values.
   *    always_multiple (bool): Boolean if attribute only accepts multiple values.
   *    translatable (boolean) : If attribute acceptce multiple lanugages.
   *    field_types (array) : array of types of fields accepted.
   *
   */
  public function getAttributes();

  /**
   * Returns an associative array of Custom product attributes.
   *
   * @return mixed
   *  Associative array of attributes keyed off of attribute name.
   *  AttributeName =>
   *    label (string) : Human display label of attribute
   *    field_types (array) : array of types of fields accepted.
   */
  public function getCustomAttributes();

  /**
   * Save custom product feed attributes to database.
   *
   * @param array $custom_attributes
   *   Array of custom attributes. Each attribute has following keys.
   *    attribute (string): Name of the attribute.
   *    label (string): Human readable label of attribute.
   *    field_types (array): Array of field types accepted by this attribute.
   * @return mixed
   */
  public function saveCustomAttributes(array $custom_attributes);

  /**
   * Delete custom product feed attributes from database.
   *
   * @param array $custom_attributes
   *   Array of custom attributes names to delete.
   *
   * @return mixed
   */
  public function deleteCustomAttributes(array $custom_attributes);
}