<?php

namespace Drupal\bazaarvoice_productfeed\Plugin\QueueWorker;
use Drupal\bazaarvoice_productfeed\Service\ProductfeedBuilderServiceInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;


abstract class ProductFeedQueueWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface   {

  protected $productfeedBuilder;

  public function __construct(ProductfeedBuilderServiceInterface $productfeed_builder) {
    $this->productfeedBuilder = $productfeed_builder;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('bazaarvoice_productfeed.builder')
    );
  }

  public function processItem($data) {
    $return_value = FALSE;

    foreach ($data as $action) {
      switch ($action['action']) {
        case 'send_productfeed':
          $return_value = $this->productfeedBuilder->sendFeed();
          break;
        case 'generate_productfeed':
          $return_value = $this->productfeedBuilder->buildFeed();
          break;
        case 'build_entity':
          $entity_type = $action['data']['entity_type'];
          $bundle_name = $action['data']['bundle_name'];
          $entity_id = $action['data']['entity_id'];
          $this->productfeedBuilder->buildProduct($entity_id, $entity_type, $bundle_name);
          $return_value = TRUE;
          break;
        default:
          // Call on other modules?
          break;
      }
    }

    return $return_value;
  }
}