<?php

namespace Drupal\bazaarvoice_productfeed\Plugin\QueueWorker;

/**
 * Processes tasks for bazaarvoice_productfeed.
 *
 * @QueueWorker(
 *   id = "bazaarvoice_productfeed_queue",
 *   title = @Translation("Bazaarvoice ProductFeed Queue worker"),
 *   cron = {"time" = 90}
 * )
 */
class ProductFeedQueueWorker extends ProductFeedQueueWorkerBase {}