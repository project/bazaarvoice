<?php

namespace Drupal\bazaarvoice_productfeed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;


class AdminSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bazaarvoice_reviews_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bazaarvoice.reviews.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bazaarvoice.productfeed.settings');

    $form['feed'] = [
      '#type' => 'details',
      '#title' => $this->t('Feed Settings'),
      '#open' => TRUE,
    ];

    $form['feed']['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Name'),
      '#required' => TRUE,
      '#description' => $this->t('The site name for the Bazaarvoice account.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('feed.site_name'),
    ];

    $form['feed']['file_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Productfeed File Name'),
      '#required' => TRUE,
      '#description' => $this->t('The prefix for the product feed filename. Can only contain letters, numbers, underscores and hyphens. The date will always be appended to the filename when prdocutfeed is generated.'),
      '#default_value' => $config->get('feed.file_name')
    ];

    $form['feed']['frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Upload Frequency'),
      '#description' => $this->t('Select frequency of SFTPing Product Feed to Bazaarvoice.'),
      '#options' => [
        0 => $this->t('Never'),
        3600 => $this->t('Hourly'),
        43200 => $this->t('Twice a Day'),
        86400 => $this->t('Daily'),
        604800 => $this->t('Weekly'),
      ],
      '#default_value' => $config->get('feed.frequency'),
    ];


    $form['sftp'] = [
      '#type' => 'details',
      '#title' => $this->t('sFTP Settings'),
      '#description' => $this->t('Settings for connecting to the Bazaarvoice SFTP for uploading product data. It is recommended not configuring these values in this form, instead configure them using $conf variables in your settings.php'),
      '#open' => TRUE,
    ];

    $form['sftp']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#description' => $this->t('Username to connect to the SFTP server.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('sftp.username'),
    ];

    $form['sftp']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
      '#description' => $this->t('Password for connecting to the SFTP server.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('sftp.password'),
    ];

    $form['sftp']['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#required' => TRUE,
      '#description' => $this->t('The port for connection to the SFTP server..'),
      '#maxlength' => 10,
      '#default_value' => $config->get('sftp.port'),
    ];

    $form['sftp']['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#required' => TRUE,
      '#description' => $this->t('The directory on the SFTP server to upload the feed files.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('sftp.directory'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory()->getEditable('bazaarvoice.productfeed.settings')
      ->set('feed.site_name', $values['site_name'])
      ->set('feed.file_name', $values['file_name'])
      ->set('feed.frequency', $values['frequency'])
      ->set('sftp.username', $values['username'])
      ->set('sftp.password', $values['password'])
      ->set('sftp.directory', $values['directory'])
      ->set('sftp.port', $values['port'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
