<?php

namespace Drupal\bazaarvoice_reviews\Service;

use BazaarvoiceConversations\BazaarvoiceConversationsInterface;
use Drupal\bazaarvoice\Service\BazaarvoiceServiceInterface;
use Drupal\bazaarvoice_products\Service\ProductsServiceInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Link;
use Drupal\Component\Utility\Html;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Routing\Route;

class ReviewsService implements ReviewsServiceInterface {

    use UseCacheBackendTrait;
    use StringTranslationTrait;

    protected $config;

    protected $method;

    protected $apiVersion;

    protected $moduleHandler;

    protected $loggerFactory;

    protected $pagerManager;

    protected $pagerParameters;

    protected $products;

    protected $bazaarvoice;

    protected $reviews;

    protected $limit;

    public function __construct($config, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory, CacheBackendInterface $cache_backend, PagerManagerInterface $pager_manager, PagerParametersInterface $pager_parameters, BazaarvoiceConversationsInterface $conversations, ProductsServiceInterface $products, BazaarvoiceServiceInterface $bazaarvoice) {        $this->config = $config->get('bazaarvoice.reviews.settings');
        $this->method = $config->get('bazaarvoice.settings')->get('method');
        $this->apiVersion = $config->get('bazaarvoice.settings')
          ->get('hosted.api_version');
        $this->moduleHandler = $module_handler;
        $this->loggerFactory = $logger_factory;
        $this->products = $products;
        $this->bazaarvoice = $bazaarvoice;
        $this->reviews = $conversations->getContentType('Reviews');
        $this->limit = $this->config->get('conversations.reviews_per_page');
    }

    public function routes() {
        $routes = [];
        $view = $write = FALSE;

        if ($this->method == 'conversations') {
            if ($reviews_uri = $this->config->get('conversations.product_reviews_uri')) {
                $view = [
                  'path' => '/{entity}/' . $reviews_uri,
                  'defaults' => [
                    '_controller' => '\Drupal\bazaarvoice_reviews\Controller\ViewReviewsController::viewReviews',
                    '_title' => 'Reviews',
                  ],
                  'requirements' => [
                    '_custom_access' => '\Drupal\bazaarvoice_reviews\Controller\viewReviewsController::accessReviews',
                  ],
                ];
            }

            if ($write_review_uri = $this->config->get('conversations.write_review_uri')) {
                $write = [
                  'path' => '/{entity}/' . $write_review_uri,
                  'defaults' => [
                    '_controller' => '\Drupal\bazaarvoice_reviews\Controller\WriteReviewController::writeAReview',
                    '_title' => 'Write a Review',
                  ],
                  'requirements' => [
                    '_custom_access' => '\Drupal\bazaarvoice_reviews\Controller\WriteReviewController::accessWriteAReview',
                  ],
                ];
            }

            // Get all entity types marked as product types.
            if ($product_types = $this->products->getProductTypes()) {
                foreach ($product_types as $entity_type => $bundles) {
                    $options = ['parameters' => ['entity' => ['type' => 'entity:' . $entity_type]]];
                    // Add view route.
                    if ($view) {
                        $route = new Route($entity_type . $view['path'], $view['defaults'], $view['requirements'], $options);
                        $route_name = 'bazaarvoice_reviews.entity.' . $entity_type . '.reviews';
                        $routes[$route_name] = $route;
                    }

                    if ($write) {
                        $route = new Route($entity_type . $write['path'], $write['defaults'], $write['requirements'], $options);
                        $route_name = 'bazaarvoice_reviews.entity.' . $entity_type . '.write_a_review';
                        $routes[$route_name] = $route;
                    }
                }
            }
        }

        return $routes;
    }

    public function getEntityWriteReviewLink(EntityInterface $entity) {
        $text = $this->t('Write a Review');

        $type = $entity->getEntityTypeId();
        $eid = $entity->id();
        $write_review_uri = $this->config->get('write_review_uri');

        $uri = 'internal:/' . $type . '/' . $eid . '/' . ($write_review_uri ?: 'write-a-review');
        $url = Url::fromUri($uri, ['absolute' => TRUE]);

        return Link::fromTextAndUrl($text, $url);
    }

    public function getEntityProductReviews(EntityInterface $entity, $page = 0) {
        $reviews = FALSE;
        // Able to get product id for this entity?
        if ($product_id = $this->products->getEntityProductId($entity)) {
            $locale = $this->bazaarvoice->getEntityLocale($entity);

            $cid = 'bazaarvoice_reviews:' . $product_id . ':' . $locale . ':' . $this->limit . ':' . $page;

            // Able to retrieve from cache?
            if ($cache = $this->cacheGet($cid)) {
                $reviews = $cache->data;
            }
            else {
                $configuration = [
                  'arguments' => [
                    'Limit' => $this->limit,
                    'Offset' => ($page * $this->limit),
                    'Locale' => $locale,
                    'filter' => [
                      'productid' => $product_id,
                    ],
                  ],
                ];

                // Able to get reviews?
                if ($response = $this->reviews->retrieveRequest($configuration)) {
                    if ($response->getResultCount()) {
                        $reviews = [
                          'reviews' => $response->getResults(),
                          'limit' => $response->getLimit(),
                          'total_results' => $response->getTotalResults(),
                        ];
                        $this->cacheSet($cid, $reviews, $this->bazaarvoice->getCacheLimit());
                    }
                }
            }
        }

        return $reviews;
    }

    public function viewEntityProductReviews(EntityInterface $entity) {
        $render = [];
        $has_permission = \Drupal::currentUser()
          ->hasPermission('view bazaarvoice reviews');
        $product_entity = \Drupal::service('bazaarvoice.products')
          ->isProductType($entity->getEntityTypeId(), $entity->bundle());

        // Has permission? and this is a product entity?
        if ($has_permission && $product_entity) {
            // What method is being used to view reviews?
            if ($this->method == 'hosted') {
                $render = $this->viewEntityHostedReviews($entity);
            }
            else {
                $render = $this->viewEntityConversationsReviews($entity);
            }
        }
        return $render;
    }

    private function viewEntityConversationsReviews(EntityInterface $entity) {
        $pager_id = rand(0, 99999);

        // Permission to submit a review?
        $review_link = '';
        if (\Drupal::currentUser()
          ->hasPermission('submit bazaarvoice reviews')) {
            $review_link = $this->getEntityWriteReviewLink($entity);
        }

        // Build render array
        $render = [
          '#theme' => 'bazaarvoice_reviews_reviews',
          '#reviews' => [],
          '#review_link' => $review_link,
          '#pager' => [],
        ];

        // Retrieve product reviews.
        if ($reviews = $this->getEntityProductReviews($entity, $this->pagerParameters->findPage($pager_id))) {
            // Add Render arrays for each review.
            foreach ($reviews['reviews'] as $review) {
                $render['#reviews'][] = [
                  '#theme' => 'bazaarvoice_reviews_review',
                  '#review' => $review,
                ];
            }
            $render['#pager'] = [
              '#theme' => 'pager',
              '#quantity' => $reviews['total_results'],
              '#element' => 0,
              '#parameters' => [],
              '#route_name' => '<none>',
            ];

            // Initialize a pager
            $this->pagerManager->createPager($reviews['total_results'], $reviews['limit'], $pager_id);
        }

        return $render;
    }

    private function viewEntityHostedReviews(EntityInterface $entity) {
        $render = [];

        if ($product_id = $this->products->getEntityProductId($entity)) {
            // Permission to submit a review?
            $review_link = '';
            if (\Drupal::currentUser()
              ->hasPermission('submit bazaarvoice reviews')) {
                $review_link = $this->getEntityWriteReviewLink($entity);
            }

            $render = [
              '#theme' => 'bazaarvoice_reviews_hosted_reviews',
              '#review_link' => $review_link,
              '#attached' => [
                'library' => [
                  'bazaarvoice/hosted.external',
                  'bazaarvoice_reviews/hosted.productid',
                ],
                'drupalSettings' => [
                  'bazaarvoiceReviews' => [
                    'productid' => $this->products->getEntityProductId($entity),
                    'apiVersion' => $this->apiVersion,
                  ],
                ],
              ],
              '#productId' => $this->products->getEntityProductId($entity),
              '#apiVersion' => $this->apiVersion,
              '#summary' => [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#attributes' => [
                  'id' => 'BVRRSummaryContainer',
                ],
              ],
              '#reviews' => [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#attributes' => [
                  'id' => 'BVRRContainer',
                ],
              ],
            ];
        }

        return $render;
    }

    public function getReviewForm(EntityInterface $entity) {
        $review_form = '';

        if ($product_id = $this->products->getEntityProductId($entity)) {
            $locale = $this->bazaarvoice->getEntityLocale($entity);

            $config = [
              'arguments' => [
                'Locale' => $locale,
                'filter' => [
                  'ProductId' => $product_id,
                ],
              ],
            ];

            if ($submission_form = $this->reviews->getSubmissionForm($config)) {
                if ($submission_form->hasErrors()) {
                    $errors = $submission_form->getErrors();
                    // @todo: set message.
                    $do = 1;
                }
                elseif (($form = $submission_form->getForm()) && ($form_data = $submission_form->getData())) {
                    $review_form = $this->buildDrupalFormElements($form, $form_data);
                }
            }
        }
        return $review_form;
    }

    public function submitReviewForm(EntityInterface $entity, array $values = []) {
        $submit_values = [];
        if ($product_id = $this->products->getEntityProductId($entity)) {
            $locale = $this->bazaarvoice->getEntityLocale($entity);

            $config = [
              'arguments' => [
                'Locale' => $locale,
                'filter' => [
                  'ProductId' => $product_id,
                ],
              ],
            ];

            $submission = $this->reviews->submitSubmission($values, $config);
            // Were there errors?
            if ($submission->hasErrors()) {
                $submit_values['errors'] = $submission->getErrors();
            }
            else {
                $submit_values['response_time'] = $submission->getTypicalHoursToPost();
            }
        }
        else {
            $submit_values['errors'][] = $this->t('Unable to submit review');
        }

        return $submit_values;
    }

    public function getReviewFormErrors(EntityInterface $entity, array $values = []) {
        $errors = [];

        if ($product_id = $this->products->getEntityProductId($entity)) {
            $locale = $this->bazaarvoice->getEntityLocale($entity);

            $config = [
              'arguments' => [
                'Locale' => $locale,
                'filter' => [
                  'ProductId' => $product_id,
                ],
              ],
            ];

            if ($submission_form = $this->reviews->getSubmissionForm($config)) {
                $form_values = [];
                $form_fields = $submission_form->getForm();
                // Loop through each field and validate.
                foreach ($form_fields as $field_id => $field_element) {
                    $value = NULL;
                    // Get the field value.
                    if (isset($values[$field_id])) {
                        $value = $values[$field_id];
                    }

                    if (!is_null($value)) {
                        // Store the value in case need to check it out later.
                        $form_values[$field_id] = $value;
                        // Validate the value.
                        if ($field_errors = $this->getFieldElementErrors($field_element, $value)) {
                            $errors[$field_id] = $field_errors;
                        }
                    }
                }

                // No errors yet?
                if (empty($errors)) {
                    // Have bazaarvoice validate.
                    $preview = $this->reviews->previewSubmission($field_values, $config);

                    // Have errors?
                    if ($form_errors = $preview->getFormErrors()) {
                        foreach ($form_errors['FieldErrors'] as $field => $error) {
                            $errors[$field] = [$error['Message']];
                        }
                    }
                    // Else, some other error occurred.
                    else {
                        $errors = ['' => t('There is an error with the data you are trying to submit.')];
                    }
                }
            }
        }

        return empty($errors) ? FALSE : $errors;
    }

    private function buildDrupalFormElements($elements, $form_data) {
        $form_elements = [];

        foreach ($elements as $element) {
            $form_element = FALSE;
            // Get element id.
            $id = $element['Id'];
            // Check if a Bazaarvoice internal field element.
            $internal = $this->isInternalElement($id);
            // Check that element exists in form data.
            $exists = isset($form_data[$element['Type'] . 's'][$id]);
            // If not internal and exists.
            if (!$internal && $exists) {
                // Build drupal element
                $form_element = $this->buildDrupalFormElement($form_data[$element['Type'] . 's'][$id], $element['Type'], $form_data);
                // If element type is fieldset, alter the id.
                if ($form_element['#type'] == 'fieldset') {
                    $id .= '_group';
                }

                // Add to elements array.
                $form_elements[$id] = $form_element;
            }
        }

        return $form_elements;
    }

    private function isInternalElement($element_id) {
        $is_internal = FALSE;
        // Create array of internal bazaarvoice fields.
        // https://developer.bazaarvoice.com/apis/conversations/tutorials/field_types#internal-fields.
        $internal_fields = [
          'HostedAuthentication_RememberMe',
          'isratingsonly',
          'IsUserAnonymous',
          'netpromoterexpanded',
          'socialconnect_facebook_usertoken',
          'socialconnect_facebook_hostname',
          'socialconnect_facebook_postresult',
          'socialconnect_facebook_postdesired',
          'socialconnect_facebook_postonsubmit',
          'userlocationgeocode_latitude',
          'userlocationgeocode_longitude',
          'userlocationgeocode_country',
          'userlocationgeocode_region',
          'userlocationgeocode_city',
        ];

        // In array of field IDs or matches 'productrecommendationcaption_[N]' ?
        if (in_array($element_id, $internal_fields) || substr($element_id, 0, 29) == 'productrecommendationcaption_') {
            $is_internal = TRUE;
        }

        return $is_internal;
    }

    private function buildDrupalFormElement($element, $type, $form_data) {
        $form_element = [];

        // Build different drupal form element based on bazaarvoice form element type.
        switch ($type) {
            // Groups = Fieldsets.
            case 'InputGroup':
                // Have sub elements?
                if (isset($element['SubElements']) && count($element['SubElements']) > 0) {
                    // Add sub array.
                    $form_element = $this->buildDrupalFormElements($element['SubElements'], $form_data);
                    // Have a label?
                    if (isset($element['Label']) && !empty($element['Label'])) {
                        // Make it a fieldset.
                        $form_element['#type'] = 'fieldset';
                    }
                }
                break;

            // Boolean to be checkbox.
            case 'BooleanInput':
                $form_element['#type'] = 'checkbox';
                break;

            // Integer to be radios, 1 to 5 values.
            case 'IntegerInput':
                $form_element['#type'] = 'radios';
                $form_element['#options'] = range(1, 5);
                break;

            // Select to be a select.
            case 'SelectInput':
                $form_element['#type'] = 'select';
                // Loop through all of the options.
                foreach ($element['Options'] as $option) {
                    $label = $option['Label'] ?: $option['Value'];
                    // If label is blank, then it is the 'select' option.
                    if (empty($label)) {
                        $label = $this->t('Select');
                    }
                    // Add to options array.
                    $form_element['#options'][$option['Value']] = $label;
                    // The selected option?
                    if ($option['Selected']) {
                        $form_element['#default'] = $option['Value'];
                    }
                }
                break;

            // Textarea is a textarea.
            case 'TextAreaInput';
                $form_element['#type'] = 'textarea';
                break;

            // Text is textfield.
            case 'TextInput':
                $form_element['#type'] = 'textfield';
                break;

            default:
                // Do nothing.
                break;
        }

        // Have a set type for this form element?
        if (isset($form_element['#type'])) {
            // Required?
            if (isset($element['Required']) && $element['Required']) {
                $form_element['#required'] = TRUE;
            }
            // Default value?
            if (isset($element['Default']) && $element['Default']) {
                $form_element['#default_value'] = $element['Default'];
            }
            // Label?
            if (isset($element['Label']) && !empty($element['Label'])) {
                $form_element['#title'] = $element['Label'];
            }
            // Else, if not a group add title based on id.
            elseif ($element['Type'] != 'InputGroup') {
                $title = str_replace('_', ' ', $element['Id']);
                $title = ucwords(trim($title));
                if (!empty($title)) {
                    $form_element['#title'] = $title;
                }
            }
            // Character count?
            if ($element['Type'] != 'InputGroup' && (isset($element['MaxLength']) || isset($element['MinLength']))) {
                $description = '';
                // Has a minlength?
                if ($element['MinLength']) {
                    $description .= $this->t('Must be at least @count characters', ['@count' => $element['MinLength']]);
                }
                // Has a maxlength?
                if ($element['MaxLength']) {
                    // Add max length to form element.
                    $form_element['#maxlength'] = $element['MaxLength'];
                    // Add to description.
                    $description = ($description ? $description . '<br />' : '');
                    $description .= $this->t('Must be no longer than @count characters', ['@count' => $element['MaxLength']]);
                }
                // Have description text?
                if ($description) {
                    // Add to form element.
                    $form_element['#description'] = $description;
                }
            }
        }

        return $form_element;
    }

    private function getFieldElementErrors($field, $value) {
        $errors = [];
        // Get the field label.
        $label = $field['Id'];
        if (isset($field['Label']) && !empty($field['Label'])) {
            $label = $field['Label'];
        }
        else {
            $label = ucwords(trim(str_replace('_', ' ', $label)));
        }

        // Validate based on field type.
        switch ($field['Type']) {
            case 'BooleanInput':
                if (!is_bool($value) && !in_array($value, [0, 1])) {
                    $errors[] = $this->t('Invalid value submitted for @field', ['@field' => $label]);
                }
                break;

            case 'IntegerInput':
                if (!in_array($value, range(0, 5))) {
                    $errors[] = $this->t('Invalid selection for @field', ['@field' => $label]);
                }
                break;

            case 'SelectInput':
                $valid_option = FALSE;
                // Loop through the options.
                foreach ($field['Options'] as $option) {
                    if ($option['Value'] == $value) {
                        $valid_option = TRUE;
                        break;
                    }
                }

                if (!$valid_option) {
                    $errors[] = t('Invalid selection for @field', ['@field' => $label]);
                }
                break;

            case 'TextAreaInput':
            case 'TextInput':
                // Check that it is a plain value.
                if (Html::escape($value) != $value) {
                    $errors[] = $this->t('Only plain text is accepted for @field', ['@field' => $label]);
                }

                // Have a min length?
                if (isset($field['MinLength']) && (strlen($value) < $field['MinLength'])) {
                    $errors[] = $this->t('@field text must be a minimum of @count characters.', [
                      '@field' => $label,
                      '@count' => $field['MinLength'],
                    ]);
                }

                // Have a max length?
                if (isset($field['MaxLength']) && (strlen($value) > $field['MaxLength'])) {
                    $errors[] = $this->t('@field text must be a maximum of @count characters.', [
                      '@field' => $label,
                      '@count' => $field['MaxLength'],
                    ]);
                }
                break;
        }

        return !empty($errors) ? $errors : FALSE;
    }

}
