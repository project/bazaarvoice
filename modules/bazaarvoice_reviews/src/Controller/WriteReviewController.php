<?php

namespace Drupal\bazaarvoice_reviews\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

class WriteReviewController extends ControllerBase {

  public function accessWriteAReview(AccountInterface $account) {
    $submit_reviews_permission = $account->hasPermission('submit bazaarvoice reviews');
    $view_content_permission = $account->hasPermission('access content');

    return AccessResult::allowedIf($submit_reviews_permission && $view_content_permission);
  }

  public function writeAReview(EntityInterface $entity) {
    $response = '';

    $entity_type = $entity->getEntityType()->getProvider();
    $bundle = $entity->bundle();
    // Check that entity is a review type.
    if (\Drupal::service('bazaarvoice.products')->isProductType($entity_type, $bundle)) {
      $method = \Drupal::config('bazaarvoice.reviews.settings')->get('method');
      if ($method == 'hosted') {
        $response = $this->writeHostedReview($entity);
      }
      else {
        $response = $this->writeConversationsReview($entity);
      }
    }
    else {
      $response = AccessResult::forbidden();
    }

    return $response;
  }

  public function reviewContainer($locale_code) {

    $response = '';

    if (\Drupal::service('bazaarvoice')->isValidLocaleCode($locale_code)) {

      $build = array(
        '#theme' => 'bazaarvoice_review_container',
        '#js_path' => \Drupal::service('bazaarvoice')->buildHostedJsPath($locale_code),
        '#canonical' => Url::fromRoute('bazaarvoice_reviews.container', ['locale_code' => $locale_code])->toString(),
      );
      $html = \Drupal::service('renderer')->renderRoot($build);
      $response = new Response();
      $response->setContent($html);

    } else {
      $response = AccessResult::forbidden();
    }

    return $response;
  }

  private function writeHostedReview(EntityInterface $entity) {
      $submission_url = $this->getSubmissionUrl($entity);
      $user_token = $this->getUserToken();

      $response = [
        '#theme' => 'bazaarvoice_reviews_hosted_new_review',
        '#review_form' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'id' => 'BVSubmissionContainer',
          ],
        ],
        '#productid' => \Drupal::service('bazaarvoice.products')->getEntityProductId($entity),
        '#attached' => [
          'library' => [
            'bazaarvoice_reviews/hosted.external',
            'bazaarvoice_reviews/hosted.writereview',
            'bazaarvoice_reivews/hosted.productid',
          ],
          'drupalSettings' => [
            'bazaarvoiceHostedReview' => [
              'submissionContainerUrl' => $submission_url,
              'submissionContainer' => ['userToken' => $user_token],
              'productid' =>\Drupal::service('bazaarvoice.products')->getEntityProductId($entity)
            ],
          ],
        ],
      ];

      return $response;
  }

  private function writeConversationsReview(EntityInterface $entity) {
    // Get the form.
    $form = array(
      '#theme' => 'bazaarvoice_reviews_new_review',
      '#form' => $form = \Drupal::formBuilder()
        ->getForm('Drupal\bazaarvoice_reviews\Form\AddReviewForm', $entity),
    );
    // Theme the output.
    return $form;
  }

  private function getSubmissionUrl(EntityInterface $entity) {
    $entity_type = $entity->getEntityType()->getProvider();
    $entity_id = $entity->id();
    $write_review_uri = \Drupal::config('bazaarvoice.reviews.settings')->get('write_review_uri');

    return Url::fromUri('internal:/' . $entity_type . '/' . $entity_id . '/' . $write_review_uri)->toString();
  }

  private function getUserToken() {
    $user = \Drupal::currentUser();

    $user_token = '';
    // Logged-in user with data?
    if ($user->uid) {
      // Get the shared key.
      $shared_key = \Drupal::config('bazaarvoice_reviews.settings')->get('hosted.shared_key');
      // Build user token.
      $user_token = array(
        'date=' . date('Ymd'),
        'userid=' . $user->uid,
        'emailaddress=' . urlencode($user->mail),
      );
      // Implode token, joint by '&'.
      $user_token = implode('&', $user_token);
      // Hash the user token.
      $user_token = md5($shared_key . $user_token) . bin2hex($user_token);
    }

    return $user_token;
  }
}