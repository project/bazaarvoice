<?php

namespace Drupal\bazaarvoice_reviews\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class ViewReviewsController extends ControllerBase {

  public function title(EntityInterface $entity) {
    return $this->entityManager->getTranslationFromContext($entity)->label();
  }

  public function accessReviews(AccountInterface $account) {

    $view_reviews_permission = $account->hasPermission('view bazaarvoice reviews');
    $view_content_permission = $account->hasPermission('access content');
    //@TODO: determine if current page has entity that is a product type.
    $is_product_type = TRUE;

    return AccessResult::allowedIf($view_reviews_permission && $view_content_permission && $is_product_type);
  }

  public function accessWriteAReview(AccountInterface $account) {
    $submit_reviews_permission = $account->hasPermission('submit bazaarvoice reviews');
    $view_content_permission = $account->hasPermission('access content');
    //@TODO: determine if current page has entity that is a product type.
    $is_product_type = TRUE;

    return AccessResult::allowedIf($submit_reviews_permission && $view_content_permission && $is_product_type);
  }

  public function viewReviews(EntityInterface $entity) {
    $response = '';

    $entity_type = $entity->getEntityType()->getProvider();
    $bundle = $entity->bundle();
    // Check that entity is a review type.
    if (\Drupal::service('bazaarvoice.products')->isProductType($entity_type, $bundle)) {
      $response = \Drupal::service('bazaarvoice.reviews')->viewEntityProductReviews($entity);
    }
    else {
      $response = AccessResult::forbidden();
    }

    return $response;
  }

}