<?php

namespace Drupal\bazaarvoice_reviews\Plugin\Block;

use Drupal\bazaarvoice_products\Plugin\Block\BazaarvoiceProductsBlock;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a list of Bazaarvoice Product Reviews.
 *
 * @Block(
 *   id = "bazaarvoice_reviews",
 *   admin_label = @Translation("Product Reviews"),
 * )
 */
class BazaarvoiceReviewsBlock extends BazaarvoiceProductsBlock {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    // Have a product entity?
    if ($entity = $this->getCurrentRouteProductEntity()) {

      $block['subject'] = t('Product Reviews');

      if ($review_content = \Drupal::service('bazaarvoice.reviews')->viewEntityProductReviews($entity)) {
        $block['content'] = $review_content;
      }
    }

    return $block;
  }

}