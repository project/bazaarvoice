<?php

namespace Drupal\bazaarvoice_reviews\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;


class AdminSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bazaarvoice_reviews_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bazaarvoice.reviews.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bazaarvoice.reviews.settings');

    if ($this->config('bazaarvoice.settings')->get('method') == 'conversations') {

      $form['conversations'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Conversations API settings'),
        '#description' => $this->t('Settings needed for Bazaarvoice reviews using the conversations API'),
      );

      $form['conversations']['reviews_per_page'] = array(
        '#type' => 'select',
        '#title' => $this->t('Reviews Per Page'),
        '#description' => $this->t('How many reviews to display per page. (Cannot exceed 100)'),
        '#default_value' => $config->get('conversations.reviews_per_page'),
        '#options' => range(1, 100),
      );

      $form['conversations']['product_reviews_uri'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Products Reviews path'),
        '#description' => $this->t('Path for viewing the reviews for a product entity. Relative to the entity page.'),
        '#default_value' => $config->get('conversations.product_reviews_uri'),
      );

      $form['conversations']['write_review_uri'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Write A Review path'),
        '#description' => $this->t('Path for the write a review page, relative to the entity page.'),
        '#required' => TRUE,
        '#default_value' => $config->get('conversations.write_review_uri')
      );
    }
    else {

      $form['hosted'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Hosted UI settings'),
        '#description' => $this->t('Settings needed for Bazaarvoice hosted reviews'),
        '#states' => array(
          'visible' => array(
            ':input[name="method"]' => array('value' => 'hosted'),
          ),
        ),
      );



      if ($locales = \Drupal::service('bazaarvoice')->getLocaleCodes()) {

        $form['hosted']['container'] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => $this->t('Review Container Pages URLs.'),
        ];

        $form['hosted']['container']['pages'] = [
          '#type' => 'table',
          '#caption' => $this->t('Url paths to <a href="@url" target="_blank">container pages</a> for review forms.', [
            '@url' => 'http://knowledge.bazaarvoice.com/wp-content/conversations/en_US/Collect/container_page.html',
          ]),
          '#header' => [
            $this->t('Language'),
            $this->t('Url'),
          ],
        ];

        foreach ($locales as $langcode => $locale) {
          $form['hosted']['container']['pages'][$langcode]['language'] = [
            '#markup' => \Drupal::languageManager()->getLanguage($langcode)->getName(),
          ];
          $form['hosted']['container']['pages'][$langcode]['url'] = [
            '#markup' => Url::fromRoute('bazaarvoice_reviews.container', ['locale_code' => $locale])->setAbsolute(TRUE)->toString(),
          ];
        }
      }
    }

    $form['url_settings'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('URL Settings'),
      '#description' => $this->t('Settings for the urls for review pages.'),
    );

    $form['url_settings']['product_reviews_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Products Reviews path'),
      '#description' => $this->t('Path for viewing the reviews for a product entity. Relative to the entity page.'),
      '#default_value' => $config->get('conversations.product_reviews_uri'),
    );

    $form['url_settings']['write_review_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Write A Review path'),
      '#description' => $this->t('Path for the write a review page, relative to the entity page.'),
      '#required' => TRUE,
      '#default_value' => $config->get('conversations.write_review_uri')
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config = $this->configFactory()->getEditable('bazaarvoice.reviews.settings');



    if ($this->config('bazaarvoice.settings')->get('method') == 'conversations') {
      $config->set('conversations.reviews_per_page', $values['reviews_per_page']);
      $config->set('conversations.product_reviews_uri', filter_var($values['product_reviews_uri'], FILTER_SANITIZE_URL));
      $config->set('conversations.write_review_uri', filter_var($values['write_review_uri'], FILTER_SANITIZE_URL));
    }
    else {
      $config->set('hosted.url', filter_var($values['hosted_url'], FILTER_SANITIZE_URL));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
