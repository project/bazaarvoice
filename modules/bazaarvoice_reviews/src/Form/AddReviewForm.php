<?php

namespace Drupal\bazaarvoice_reviews\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class AddReviewForm extends FormBase {

  public function title(EntityInterface $entity) {
    return $this->entityManager->getTranslationFromContext($entity)->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bazaarvoice_reviews_add_review_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entity = $form_state->getBuildInfo()['args'][0];
    $form = \Drupal::service('bazaarvoice.reviews')->getReviewForm($entity);
    $form['#entity'] = $entity;

    // Add device fingerprinting.
    // See: https://developer.bazaarvoice.com/apis/conversations/tutorials/device_fingerprinting
    $form['fp'] = array(
      '#type' => 'hidden',
      '#attributes' => array(
        'id' => 'fp',
        'value' => '',
      ),
      '#attached' => [
        'library' => [
          'bazaarvoice_reviews/conversations.snare.config',
        ],
      ],
    );

    $form['actions'] = array('#type' => 'actions');
    // Submit button.
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 1000,
    );

    // Set previous values.
    $this->setFormValues($form, $form_state->getValues());

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the conversation fields. Any errors?
    $entity = $form_state->getBuildInfo()['args'][0];
    if ($errors = \Drupal::service('bazaarvoice.reviews')->getFormErrors($entity, $form_state->getValues())) {
      // Loop through all of the errors.
      foreach ($errors as $field_id => $field_errors) {
        // For each error on a field.
        foreach ($field_errors as $error) {
          // Print a form error.
          $form_state->setErrorByName($field_id, Html::escape($error));
        }
      }
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $form_state->getBuildInfo()['args'][0];
    if ($submission = \Drupal::service('bazaarvoice.reviews')->submitReviewForm($entity, $form_state->getValues())) {
      // Any errors?
      if (isset($submission['errors'])) {
        // Print errors.
        foreach ($submission['errors'] as $error) {
          $this->messenger()->addError(Html::escape($error));
        }
      }
      else {
        $message = t('Your review has been submitted.');
        if (isset($submission['response_time'])) {
          $message .= $this->t('Reviews can take up to @hours hours to display', ['@hours' => $submission['response_time']]);
        }

        $this->messenger()->addMessage(Html::escape($message));
        // Redirect to product page?
        $this->redirect($form['#entity']->toUrl());
      }
    }
    // Not able to submit, return generic error.
    else {
      $this->messenger()->addError(t('There was an error submitting your review'));
    }
  }

  private function setFormValues(&$form, array $values = []) {
    foreach ($form as $field_id => &$field) {
      // Field Id an attribute from a recursive call?
      if (substr($field_id, 0, 1) == '#') {
        // Skip it.
        continue;
      }
      // Field has a previous value?
      if (isset($values[$field_id])) {
        // Set it as the default.
        $field['#default_value'] = $values[$field_id];
      }
      // Does field have children?
      if (Element::children($field)) {
        // Recursive call for children.
        $this->setFormValues($field, $values);
      }
    }
  }


}
