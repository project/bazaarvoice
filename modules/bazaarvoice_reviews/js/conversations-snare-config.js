/**
 * @file
 * Contains helper js for BV Reviews Snare fingerprinting.
 */
(function ($) {
  "use strict";

  Drupal.behaviors.bvReviewsSnare = {
    attach: function (context, settings) {
      // Inline fingerprint settings
      window.io_bbout_element_id = 'fp';
      window.io_install_stm = false;
      window.io_exclude_stm = 12;
      window.io_install_flash = false;
      window.io_enable_rip = true;
    }
  };

})(jQuery);