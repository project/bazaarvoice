(function ($) {
  "use strict";

  Drupal.behaviors.bazaarvoiceHostedWriteReview = {
    attach: function (context, settings) {
      var submissionContainerUrl = drupalSettings.bazaarvoiceHostedReview.submissionContainerUrl;
      var submission_container = drupalSettings.bazaarvoiceHostedReview.submissionContainer.userToken;
      $BV.ui('rr', 'submit_review', { productId : drupalSettings.bazaarvoiceHostedReview.productid });
    }
  };

})(jQuery);